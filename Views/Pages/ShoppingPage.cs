﻿using BotShopper.Lib;
using BotShopper.Lib.Cache;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Decal.Filters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using VirindiViewService;
using VirindiViewService.Controls;

namespace BotShopper.Views.Pages {
    class ShoppingPage : IDisposable {

        MainView mainView;

        HudList UIShoppingList;
        HudButton UIShoppingListCheckPoints;
        HudButton UIShoppingListAddSelectedToTrade;

        HudStaticText UIShoppingListHeaderMatchesRule;
        HudFixedLayout UIShoppingListLayout;

        int matchingCount = 0;
        int nonMatchingCount = 0;

        const int COL_ICON = 0;
        const int COL_NAME = 1;
        const int COL_RULE = 2;
        const int COL_PRICE = 3;
        const int COL_BUY = 4;
        const int COL_ID = 5;

        int lastPriceCheckId = 0;
        string lastPriceCheckName = "";
        bool isDraggingRuleHeader = false;

        List<string> pricesChecked = new List<string>();
        FileService service;

        public ShoppingPage(MainView mainView) {
            try {
                this.mainView = mainView;
                service = Globals.Core.Filter<FileService>();

                UIShoppingList = (HudList)mainView.view["UIShoppingList"];
                UIShoppingListCheckPoints = (HudButton)mainView.view["UIShoppingListCheckPoints"];
                UIShoppingListAddSelectedToTrade = (HudButton)mainView.view["UIShoppingListAddSelectedToTrade"];
                UIShoppingListHeaderMatchesRule = (HudStaticText)mainView.view["UIShoppingListHeaderMatchesRule"];
                UIShoppingListLayout = (HudFixedLayout)mainView.view["UIShoppingListLayout"];

                UIShoppingListCheckPoints.Hit += UIShoppingListCheckPoints_Hit;
                UIShoppingListAddSelectedToTrade.Hit += UIShoppingListAddSelectedToTrade_Hit;
                UIShoppingList.Click += UIShoppingList_Click;

                UIShoppingListHeaderMatchesRule.MouseEvent += UIShoppingListHeaderMatchesRule_MouseEvent;

                Globals.Plugin.tradeManager.TradeStarted += TradeManager_TradeStarted;
                Globals.Plugin.tradeManager.TradeEnded += TradeManager_TradeEnded;
                Globals.Plugin.tradeManager.TradeItemAdded += TradeManager_TradeItemAdded;
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void UIShoppingListHeaderMatchesRule_MouseEvent(object sender, ControlMouseEventArgs e) {
            try {
                switch (e.EventType) {
                    case ControlMouseEventArgs.MouseEventType.MouseDown:
                        isDraggingRuleHeader = true;
                        break;

                    case ControlMouseEventArgs.MouseEventType.MouseUp:
                        isDraggingRuleHeader = false;
                        break;

                    case ControlMouseEventArgs.MouseEventType.MouseMove:
                        if (!isDraggingRuleHeader) return;

                        var moved = e.X - e.InitialX;
                        var oldRect = UIShoppingListLayout.GetControlRect(UIShoppingListHeaderMatchesRule);
                        var newRect = new Rectangle(oldRect.X + moved, oldRect.Y, oldRect.Width, oldRect.Height);

                        var nameColInfo = UIShoppingList.GetColumnInfo(COL_NAME);
                        var ruleColInfo = UIShoppingList.GetColumnInfo(COL_RULE);

                        UIShoppingListLayout.SetControlRect(UIShoppingListHeaderMatchesRule, newRect);
                        UIShoppingList.SetColumnWidth(COL_NAME, nameColInfo.Width + moved);
                        UIShoppingList.SetColumnWidth(COL_RULE, ruleColInfo.Width - moved);

                        break;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void UIShoppingListAddSelectedToTrade_Hit(object sender, EventArgs e) {
            try {
                var traderId = Globals.Plugin.tradeManager.TradePartnerId;

                if (traderId == 0) {
                    Util.WriteToChat("No trade partner!");
                    return;
                }
                CoreManager.Current.Actions.TradeReset();

                var addedItemCount = 0;

                for (var i = 0; i < UIShoppingList.RowCount; i++) {
                    HudList.HudListRowAccessor row = UIShoppingList[i];

                    if (((HudCheckBox)row[COL_BUY]).Checked) {
                        var id = 0;
                        if (Int32.TryParse(((HudStaticText)row[COL_ID]).Text, out id)) {
                            if (id == 0) return;
                            var cwo = ItemCache.Get(id);
                            ChatBox.Tell(traderId, $"add {cwo.GetTradeName()}");
                            addedItemCount++;
                        }
                    }
                }

                if (addedItemCount > 0) {
                    //ChatBox.Tell(traderId, "total");
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void UIShoppingListCheckPoints_Hit(object sender, EventArgs e) {
            try {
                ChatBox.Tell(Globals.Plugin.tradeManager.TradePartnerId, "points");
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void UIShoppingList_Click(object sender, int row, int col) {
            try {
                int id = 0;
                HudList.HudListRowAccessor clickedRow = UIShoppingList[row];
                if (clickedRow == null || !int.TryParse(((HudStaticText)clickedRow[COL_ID]).Text, out id)) return;

                if (col == COL_NAME || col == COL_RULE) {
                    CoreManager.Current.Actions.SelectItem(id);
                    CoreManager.Current.Actions.RequestId(id);
                    /*
                    var cachedItem = ItemCache.Get(id);
                    Util.WriteToChat($"{cachedItem.Name} has {cachedItem.Palettes.Count} color palettes:");
                    var i = 0;
                    foreach (var p in cachedItem.Palettes) {
                        Util.WriteToChat($"  [{i}] {p.ExampleColor.Name} #{p.RGB} {p.Palette} {p.Offset} {p.Length}");
                        i++;
                    }
                    */
                }
                else if (col == COL_ICON) {
                    ((HudCheckBox)clickedRow[COL_BUY]).Checked = !((HudCheckBox)clickedRow[COL_BUY]).Checked;
                    var item = ItemCache.Get(id);

                    if (((HudCheckBox)clickedRow[COL_BUY]).Checked == true && !pricesChecked.Contains(item.GetGameDisplayName())) {
                        CheckItemPrice(id);
                    }
                }
                else if (col == COL_PRICE) {
                    CheckItemPrice(id);
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void CheckItemPrice(int id) {
            var item = ItemCache.Get(id);
            lastPriceCheckId = item.Id;
            lastPriceCheckName = item.GetGameDisplayName();
            pricesChecked.Add(item.GetGameDisplayName());
            ChatBox.Tell(Globals.Plugin.tradeManager.TradePartnerId, $"check {item.GetTradeName()}");
        }

        private void TradeManager_TradeItemAdded(object sender, TradeItemWithDataAddedEventArgs e) {
            if (e.Added == null || string.IsNullOrEmpty(e.Added.Name)) return;

            HudList.HudListRowAccessor newRow = null;

            for (var i = 0; i < UIShoppingList.RowCount; i++) {
                HudList.HudListRowAccessor row = UIShoppingList[i];

                if (((HudStaticText)row[5]).Text == e.Added.Id.ToString()) {
                    newRow = row;
                    break;
                }
            }

            if (newRow == null) {
                newRow = UIShoppingList.InsertRow(e.MatchingRule != null ? matchingCount + 1 : matchingCount + 1 + nonMatchingCount + 1);
            }

            var reason = "No Match";

            if (e.MatchingRule != null) {
                if (String.IsNullOrEmpty(e.Reason)) {
                    reason = e.MatchingRule.Name;
                }
                else {
                    reason = $"{e.MatchingRule.Name}: {e.Reason}";
                }
            }

            var name = Util.GetFullItemName(e.Added);
            var color = mainView.view.Theme.GetColor("ListText");
            color = e.Added.IsFromCache ? ChangeColorBrightness(color, -0.2f) : color;

            ((HudPictureBox)newRow[0]).Image = GetIconForItem(e.Added);
            ((HudStaticText)newRow[1]).Text = e.Added.IsFromCache ? name : $"[NEW] {name}";
            ((HudStaticText)newRow[1]).TextColor = color;
            ((HudStaticText)newRow[2]).Text = reason;
            ((HudStaticText)newRow[2]).TextColor = color;
            ((HudStaticText)newRow[3]).Text =  "??";
            ((HudStaticText)newRow[3]).TextColor = color;
            ((HudStaticText)newRow[5]).Text = e.Added.Id.ToString();

            if (e.MatchingRule == null) {
                nonMatchingCount++;
            }
            else {
                matchingCount++;
            }
        }

        private void TradeManager_TradeEnded(object sender, Decal.Adapter.Wrappers.EndTradeEventArgs e) {
            CoreManager.Current.IDQueue.DeleteCaller(Util.GetAssembly());
            CoreManager.Current.ChatBoxMessage -= Current_ChatBoxMessage;
        }

        private void TradeManager_TradeStarted(object sender, Decal.Adapter.Wrappers.EnterTradeEventArgs e) {
            if (e.TraderId == CoreManager.Current.CharacterFilter.Id) {
                UIShoppingList.ClearRows();
                pricesChecked.Clear();
                matchingCount = 0;
                nonMatchingCount = 0;

                CoreManager.Current.IDQueue.DeleteCaller(Util.GetAssembly());
                CoreManager.Current.ChatBoxMessage += Current_ChatBoxMessage;

                HudList.HudListRowAccessor matchingRow = UIShoppingList.AddRow();
                ((HudPictureBox)matchingRow[COL_ICON]).Image = 0x0600507D;
                ((HudStaticText)matchingRow[COL_NAME]).Text = "-- Matching Items " + (new string('-', 100));
                ((HudStaticText)matchingRow[COL_RULE]).Text = new string('-', 100);
                ((HudStaticText)matchingRow[COL_PRICE]).Text = "";
                //((HudCheckBox)matchingRow[COL_BUY]).Visible = false;

                HudList.HudListRowAccessor nonMatchingRow = UIShoppingList.AddRow();
                ((HudPictureBox)nonMatchingRow[COL_ICON]).Image = 0x0600507D;
                ((HudStaticText)nonMatchingRow[COL_NAME]).Text = "-- Non Matching Items " + (new string('-', 100));
                ((HudStaticText)nonMatchingRow[COL_RULE]).Text = new string('-', 100);
                ((HudStaticText)nonMatchingRow[COL_PRICE]).Text = "";
                //((HudCheckBox)nonMatchingRow[COL_BUY]).
            }
            else {
                Util.WriteToChat($"{e.TraderId} started a trade with me");
            }
        }

        private ACImage GetIconForItem(CachedWorldObject wo) {
            try {
                var bmp = new Bitmap(32, 32, PixelFormat.Format32bppArgb);
                var bitmaps = new List<Bitmap>();

                var icons = new List<int>() {
                    wo.Values(LongValueKey.IconUnderlay, -1),
                    wo.Values(LongValueKey.Icon, -1),
                    wo.Values(LongValueKey.IconOverlay, -1),
                    wo.Values(LongValueKey.IconOutline, -1)
                };

                foreach (var icon in icons) {
                    if (icon <= 0) continue;

                    var overlayImage = TryGetIcon(icon);

                    if (overlayImage == null) continue;

                    bmp = Superimpose(bmp, overlayImage);
                }

                return bmp;
            }
            catch (Exception ex) { Util.LogException(ex); }

            return new ACImage(wo.Values(LongValueKey.Icon));
        }
        public Bitmap Superimpose(Bitmap source, Bitmap target) {
            Graphics g = Graphics.FromImage(source);
            g.CompositingMode = CompositingMode.SourceOver;
            target.MakeTransparent(Color.White);
            g.DrawImage(target, new Point(0, 0));
            return source;
        }

        private Bitmap TryGetIcon(int id) {
            try {
                byte[] portalFile = service.GetPortalFile(0x06000000 + id);
                byte[] bytes = portalFile.Skip(28).Take(4096).ToArray();

                Bitmap bmp = new Bitmap(32, 32, PixelFormat.Format32bppArgb);

                BitmapData bmpData = bmp.LockBits(
                                     new Rectangle(0, 0, bmp.Width, bmp.Height),
                                     ImageLockMode.WriteOnly, bmp.PixelFormat);

                Marshal.Copy(bytes, 0, bmpData.Scan0, bytes.Length);
                bmp.UnlockBits(bmpData);

                return bmp;
            }
            catch (Exception ex) { }

            return null;
        }

        // Sunnuj Low Wield Weapons tells you, "// Agate Lightning Cestus is 3 points."
        private static readonly Regex BotTellsYouPrice = new Regex("^<Tell:IIDString:[0-9]+:(?<name>[\\w\\s'-]+)>[\\w\\s'-]+<\\\\Tell> tells you, \"/?/?(?<item>.*) is (?<points>\\d+) points.\"$");

        private void Current_ChatBoxMessage(object sender, ChatTextInterceptEventArgs e) {
            try {
                if (!Globals.Plugin.tradeManager.IsTrading) return;

                if (BotTellsYouPrice.IsMatch(e.Text)) {
                    var match = BotTellsYouPrice.Match(e.Text);
                    var itemName = match.Groups["item"].Value.Trim();

                    if (match.Groups["name"].Value != Globals.Plugin.tradeManager.GetTradePartnerName()) return;

                    UpdateLastCheckedPrice(itemName, match.Groups["points"].Value);

                    Util.WriteToChat($"{match.Groups["name"]} told you {match.Groups["item"]} costs {match.Groups["points"]} points");
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void UpdateLastCheckedPrice(string itemName, string price) {
            try {
                if (itemName == lastPriceCheckName) {
                    for (var i = 0; i < UIShoppingList.RowCount; i++) {
                        HudList.HudListRowAccessor clickedRow = UIShoppingList[i];

                        if (((HudStaticText)clickedRow[COL_ID]).Text == lastPriceCheckId.ToString()) {
                            ((HudStaticText)clickedRow[COL_PRICE]).Text = price;
                            lastPriceCheckId = 0;
                            lastPriceCheckName = "";
                            return;
                        }
                    }
                }
            }
            catch (Exception ex) {
                Util.LogException(ex);
            }
        }

        //https://www.pvladov.com/2012/09/make-color-lighter-or-darker.html
        public static Color ChangeColorBrightness(Color color, float correctionFactor) {
            float red = (float)color.R;
            float green = (float)color.G;
            float blue = (float)color.B;

            if (correctionFactor < 0) {
                correctionFactor = 1 + correctionFactor;
                red *= correctionFactor;
                green *= correctionFactor;
                blue *= correctionFactor;
            }
            else {
                red = (255 - red) * correctionFactor + red;
                green = (255 - green) * correctionFactor + green;
                blue = (255 - blue) * correctionFactor + blue;
            }

            return Color.FromArgb(color.A, (int)red, (int)green, (int)blue);
        }

        private bool disposed;

        public void Dispose() {
            try {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        protected virtual void Dispose(bool disposing) {
            try {
                if (!disposed) {
                    if (disposing) {

                    }

                    disposed = true;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}
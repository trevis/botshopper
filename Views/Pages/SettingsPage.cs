﻿using BotShopper.Lib;
using System;
using System.Globalization;
using System.IO;
using VirindiViewService.Controls;

namespace BotShopper.Views.Pages {
    class SettingsPage : IDisposable {

        MainView mainView;

        HudCheckBox UISettingsAutoCheckPointValues;
        HudCheckBox UISettingsAutoOpenPluginWindowOnTrade;
        HudCheckBox UISettingsHideDuplicateCommonItems;
        HudButton UISettingsClearItemCache;

        public SettingsPage(MainView mainView) {
            try {
                this.mainView = mainView;

                UISettingsAutoCheckPointValues = (HudCheckBox)mainView.view["UISettingsAutoCheckPointValues"];
                UISettingsAutoOpenPluginWindowOnTrade = (HudCheckBox)mainView.view["UISettingsAutoOpenPluginWindowOnTrade"];
                UISettingsHideDuplicateCommonItems = (HudCheckBox)mainView.view["UISettingsHideDuplicateCommonItems"];
                UISettingsClearItemCache = (HudButton)mainView.view["UISettingsClearItemCache"];

                UISettingsAutoCheckPointValues.Checked = Globals.Config.AutoCheckPointValues;
                UISettingsAutoOpenPluginWindowOnTrade.Checked = Globals.Config.AutoOpenPluginWindowOnTrade;
                UISettingsHideDuplicateCommonItems.Checked = Globals.Config.HideDuplicateCommonItems;

                UISettingsAutoCheckPointValues.Change += UISettingsAutoCheckPointValues_Change;
                UISettingsAutoOpenPluginWindowOnTrade.Change += UISettingsAutoOpenPluginWindowOnTrade_Change;
                UISettingsHideDuplicateCommonItems.Change += UISettingsHideDuplicateCommonItems_Change;

                UISettingsClearItemCache.Hit += UISettingsClearItemCache_Hit;
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void UISettingsClearItemCache_Hit(object sender, EventArgs e) {
            try {
                DirectoryInfo dirInfo = new DirectoryInfo(Util.GetCacheStoragePath());
                FileInfo[] files = dirInfo.GetFiles("*.json");

                Util.WriteToChat($"Clearing {files.Length} cache files.");
                foreach (var file in files) {
                    file.Delete();
                }
                Util.WriteToChat("Done.");
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void UISettingsHideDuplicateCommonItems_Change(object sender, EventArgs e) {
            try {
                Globals.Config.HideDuplicateCommonItems = UISettingsHideDuplicateCommonItems.Checked;
                Globals.Config.Save();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void UISettingsAutoOpenPluginWindowOnTrade_Change(object sender, EventArgs e) {
            try {
                Globals.Config.AutoOpenPluginWindowOnTrade = UISettingsAutoOpenPluginWindowOnTrade.Checked;
                Globals.Config.Save();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void UISettingsAutoCheckPointValues_Change(object sender, EventArgs e) {
            try {
                Globals.Config.AutoCheckPointValues = UISettingsAutoCheckPointValues.Checked;
                Globals.Config.Save();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private bool disposed;

        public void Dispose() {
            try {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        protected virtual void Dispose(bool disposing) {
            try {
                if (!disposed) {
                    if (disposing) {

                    }

                    disposed = true;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}
﻿using BotShopper.Lib;
using BotShopper.Lib.Conditions;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using VirindiViewService.Controls;
using VTClassic;

namespace BotShopper.Views.Pages {
    class FiltersPage : IDisposable {

        MainView mainView;

        HudList UIFiltersRuleList;
        HudList UIFiltersConditionList;
        HudButton UIFiltersNewCondition;
        HudButton UIFiltersNewRule;
        internal HudTextBox UIFiltersRuleName;
        internal HudFixedLayout UIFiltersConditionLayout;

        private Rule currentlyEditingRule;
        private int currentlyEditingRuleIndex = -1;
        private Condition currentlyEditingCondition;
        private int currentlyEditingConditionIndex = -1;

        public FiltersPage(MainView mainView) {
            try {
                this.mainView = mainView;

                UIFiltersRuleList = (HudList)mainView.view["UIFiltersRuleList"];
                UIFiltersConditionList = (HudList)mainView.view["UIFiltersConditionList"];
                UIFiltersRuleName = (HudTextBox)mainView.view["UIFiltersRuleName"];
                UIFiltersConditionLayout = (HudFixedLayout)mainView.view["UIFiltersConditionLayout"];
                UIFiltersNewCondition = (HudButton)mainView.view["UIFiltersNewCondition"];
                UIFiltersNewRule = (HudButton)mainView.view["UIFiltersNewRule"];

                PopulateRules();

                UIFiltersRuleList.Click += UIFiltersRuleList_Click;
                UIFiltersConditionList.Click += UIFiltersConditionList_Click;
                UIFiltersNewCondition.Hit += UIFiltersNewCondition_Hit;
                UIFiltersNewRule.Hit += UIFiltersNewRule_Hit;

                UIFiltersRuleName.Change += UIFiltersRuleName_Change;
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void LoadRule(Rule rule) {
            currentlyEditingRule = rule;
            currentlyEditingCondition = null;
            currentlyEditingConditionIndex = -1;
            UIFiltersRuleName.Text = rule.Name;
            Condition.ClearControls(UIFiltersConditionLayout);

            PopulateRuleConditions();
            PopulateRules();
        }

        internal void PopulateRuleConditions() {
            var scrollPosition = UIFiltersConditionList.ScrollPosition;
            var isMaxScrolled = UIFiltersConditionList.ScrollPosition == UIFiltersConditionList.MaxScroll;
            UIFiltersConditionList.ClearRows();

            var i = 0;
            foreach (var condition in currentlyEditingRule.conditions) {
                HudList.HudListRowAccessor newRow = UIFiltersConditionList.AddRow();

                ((HudStaticText)newRow[0]).Text = currentlyEditingConditionIndex == i ? $"> {condition.ToString()}" : condition.ToString();
                ((HudPictureBox)newRow[1]).Image = 0x060011F8; // delete

                i++;
            }

            UIFiltersConditionList.ScrollPosition = isMaxScrolled ? UIFiltersConditionList.MaxScroll : scrollPosition;

            SaveCurrentRule();
        }

        private void SaveCurrentRule() {
            if (currentlyEditingRule != null) {
                Globals.Plugin.ruleManager.AddOrUpdateRule(currentlyEditingRule);
            }
        }

        internal void ReplaceSelectedCondition(Condition condition) {
            try {
                if (currentlyEditingRule != null && currentlyEditingCondition != null) {
                    currentlyEditingRule.conditions[currentlyEditingConditionIndex] = condition;
                    PopulateRuleConditions();
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        internal void PopulateRules() {
            try {
                var scrollPosition = UIFiltersRuleList.ScrollPosition;
                var isMaxScrolled = UIFiltersRuleList.ScrollPosition == UIFiltersRuleList.MaxScroll;

                UIFiltersRuleList.ClearRows();
                var rules = Globals.Plugin.ruleManager.rules;
                var i = 0;
                foreach (var rule in rules) {
                    HudList.HudListRowAccessor newRow = UIFiltersRuleList.AddRow();

                    var isSelected = currentlyEditingRuleIndex == i;

                    ((HudStaticText)newRow[0]).Text = isSelected ? $"> {rule.Name}" : rule.Name;
                    ((HudPictureBox)newRow[1]).Image = i == 0 ? 100680819 : 100673788; // up arrow
                    ((HudPictureBox)newRow[2]).Image = i == rules.Count-1 ? 100680819 : 100673789; // down arrow
                    ((HudPictureBox)newRow[3]).Image = 0x060011F8; // delete

                    i++;
                }

                UIFiltersRuleList.ScrollPosition = isMaxScrolled ? UIFiltersRuleList.MaxScroll : scrollPosition;
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void UIFiltersNewRule_Hit(object sender, EventArgs e) {
            try {
                LoadRule(new Rule(Globals.Plugin.ruleManager.GetNextAvailableRuleName()));
                SaveCurrentRule();
                currentlyEditingRuleIndex = UIFiltersRuleList.RowCount;
                UIFiltersRuleList.ScrollPosition = UIFiltersRuleList.MaxScroll;
                PopulateRules();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void UIFiltersRuleName_Change(object sender, EventArgs e) {
            try {
                if (currentlyEditingRule != null) {
                    currentlyEditingRule.Name = UIFiltersRuleName.Text;
                    SaveCurrentRule();
                    PopulateRules();
                }
                else {
                    currentlyEditingRule = new Rule(UIFiltersRuleName.Text);
                    SaveCurrentRule();
                    PopulateRules();
                    currentlyEditingRuleIndex = UIFiltersRuleList.RowCount;
                    UIFiltersRuleList.ScrollPosition = UIFiltersRuleList.MaxScroll;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void UIFiltersRuleList_Click(object sender, int row, int col) {
            try {
                HudList.HudListRowAccessor clickedRow = UIFiltersRuleList[row];
                var rule = Globals.Plugin.ruleManager.rules[row];

                if (rule == null) {
                    return;
                }

                if (col == 0) {
                    currentlyEditingRuleIndex = row;
                    LoadRule(rule);
                }
                else if (col == 1 && row > 0) { // up
                    Globals.Plugin.ruleManager.MoveRuleUp(row);
                    if (currentlyEditingRuleIndex == row) currentlyEditingRuleIndex--;
                    PopulateRules();
                }
                else if (col == 2 && row < UIFiltersRuleList.RowCount-1) { // down
                    Globals.Plugin.ruleManager.MoveRuleDown(row);
                    if (currentlyEditingRuleIndex == row) currentlyEditingRuleIndex++;
                    PopulateRules();
                }
                else if (col == 3) { // remove
                    Globals.Plugin.ruleManager.RemoveRule(row);

                    if (row == currentlyEditingRuleIndex) {
                        ClearRule();
                    }
                    else if (row < currentlyEditingRuleIndex) {
                        currentlyEditingRuleIndex--;
                    }

                    PopulateRules();
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void ClearRule() {
            currentlyEditingRule = null;
            currentlyEditingRuleIndex = -1;
            currentlyEditingCondition = null;
            currentlyEditingConditionIndex = -1;

            UIFiltersRuleName.Text = "";
            UIFiltersConditionList.ClearRows();
            Condition.ClearControls(UIFiltersConditionLayout);
        }

        private void UIFiltersConditionList_Click(object sender, int row, int col) {
            try {
                if (currentlyEditingRule == null) return;
            
                if (col == 0) { // edit
                    var condition = currentlyEditingRule.conditions[row];
                    currentlyEditingCondition = condition;
                    currentlyEditingConditionIndex = row;
                    PopulateRuleConditions();

                    condition.DrawUI(UIFiltersConditionLayout);
                }
                else { // delete
                    currentlyEditingRule.conditions.RemoveAt(row);

                    if (row == currentlyEditingConditionIndex) {
                        Condition.ClearControls(UIFiltersConditionLayout);
                    }
                    else if (row < currentlyEditingConditionIndex) {
                        currentlyEditingConditionIndex--;
                    }

                    PopulateRuleConditions();
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void UIFiltersNewCondition_Hit(object sender, EventArgs e) {
            try {
                if (currentlyEditingRule != null) {
                    currentlyEditingCondition = new LongKeyValueCondition();
                    currentlyEditingRule.AddCondition(currentlyEditingCondition);
                    currentlyEditingConditionIndex = UIFiltersConditionList.RowCount;
                    PopulateRuleConditions();
                    currentlyEditingCondition.DrawUI(UIFiltersConditionLayout);
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private bool disposed;

        public void Dispose() {
            try {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        protected virtual void Dispose(bool disposing) {
            try {
                if (!disposed) {
                    if (disposing) {

                    }

                    disposed = true;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}
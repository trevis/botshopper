﻿using BotShopper.Lib;
using BotShopper.Views.Pages;
using Decal.Adapter;
using System;
using System.Globalization;
using VirindiViewService.Controls;

namespace BotShopper.Views {
    internal class MainView : IDisposable {
        public readonly VirindiViewService.ViewProperties properties;
        public readonly VirindiViewService.ControlGroup controls;
        public readonly VirindiViewService.HudView view;

        public FiltersPage filtersPage;
        public SettingsPage settingsPage;
        public ShoppingPage shoppingPage;

        public MainView() {
            try {
                VirindiViewService.XMLParsers.Decal3XMLParser parser = new VirindiViewService.XMLParsers.Decal3XMLParser();
                parser.ParseFromResource("BotShopper.Views.mainView.xml", out properties, out controls);
                
                view = new VirindiViewService.HudView(properties, controls);
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        internal void Init() {
            filtersPage = new FiltersPage(this);
            settingsPage = new SettingsPage(this);
            shoppingPage = new ShoppingPage(this);

            Globals.Plugin.tradeManager.IdQueueChanged += TradeManager_IdQueueChanged;
        }

        private void TradeManager_IdQueueChanged(object sender, IdQueueChangedEventArgs e) {
            try {
                int myTotal = CoreManager.Current.IDQueue.GetActionsForCaller(Util.GetAssembly()).Count;
                int total = CoreManager.Current.IDQueue.ActionCount;
                view.Title = $"{Globals.PluginName} BETA: id queue left: {myTotal}";
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private bool disposed;

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (!disposed) {
                if (disposing) {

                    if (filtersPage != null) filtersPage.Dispose();
                    if (settingsPage != null) settingsPage.Dispose();
                    if (shoppingPage != null) shoppingPage.Dispose();

                    if (view != null) view.Dispose();
                }
                
                disposed = true;
            }
        }
    }
}

## [BotShopperInstaller-1.0.1.1.exe](/uploads/8e5685f83c527beb0f2399da9c8d966c/BotShopperInstaller-1.0.1.1.exe)
* Fixed bug in icon generation


## [BotShopperInstaller-1.0.1.0.exe](/uploads/41eaef5c5a9d058db781b04062bf415d/BotShopperInstaller-1.0.1.0.exe)
* Shopping list icons now show underlays/overlays
* You can drag the "Rule Match" header along the x axis to resize shopping list columns
* Fixed a bug in shopping list display when no rules require id data
* Always show salvage workmanship
* Highlight *new* (non-cached) items.  This does not work if none of your rules require id data (TODO)


## [BotShopperInstaller-1.0.0.1.exe](/uploads/c6784bc0ba183a8fa0b7fed88ca1a5df/BotShopperInstaller-1.0.0.1.exe)
* Config option to manage plugin window now also closes the window when trade closes
* Added Icon


## [BotShopperInstaller-1.0.0.0.exe](/uploads/6940c0dd75577b79f414587f80016495/BotShopperInstaller-1.0.0.0.exe)
* Initial
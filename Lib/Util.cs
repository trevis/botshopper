﻿using System;
using System.IO;
using System.Xml;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using Decal.Adapter.Wrappers;
using Decal.Filters;
using BotShopper.Lib.Cache;
using Decal.Adapter;

namespace BotShopper.Lib {
    internal static class Util {
        internal static void CreatePluginDirectories() {
            Directory.CreateDirectory(GetPluginStoragePath());
            Directory.CreateDirectory(GetRulesStoragePath());
            Directory.CreateDirectory(GetBaseCacheStoragePath());
        }

        internal static string GetPluginStoragePath() {
            string PluginPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + @"\Decal Plugins\" + Globals.PluginName + @"\";
            return PluginPath;
        }

        internal static string GetLootProfilesStoragePath() {
            return @"C:\Games\VirindiPlugins\VirindiTank\";
        }

        internal static string GetRulesStoragePath() {
            return Path.Combine(GetPluginStoragePath(), "rules");
        }

        internal static string GetBaseCacheStoragePath() {
            return Path.Combine(GetPluginStoragePath(), "cache");
        }

        internal static string GetCacheStoragePath() {
            return Path.Combine(GetBaseCacheStoragePath(), CoreManager.Current.CharacterFilter.Server);
        }

        internal static string GetItemCachePath(int id) {
            return Path.Combine(GetCacheStoragePath(), $"{id}.json");
        }

        public static string GetResourcesDirectory() {
            string assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            return Path.Combine(assemblyFolder, "Resources");
        }

        internal static void LogException(Exception ex) {
            try {
                using (StreamWriter writer = new StreamWriter(Path.Combine(GetPluginStoragePath(), "exceptions.txt"), true)) {
                    writer.WriteLine("============================================================================");
                    writer.WriteLine(DateTime.Now.ToString());
                    writer.WriteLine("Error: " + ex.Message);
                    writer.WriteLine("Source: " + ex.Source);
                    writer.WriteLine("Stack: " + ex.StackTrace);
                    if (ex.InnerException != null) {
                        writer.WriteLine("Inner: " + ex.InnerException.Message);
                        writer.WriteLine("Inner Stack: " + ex.InnerException.StackTrace);
                    }
                    writer.WriteLine("============================================================================");
                    writer.WriteLine("");
                    writer.Close();

                    Util.WriteToChat("Error: " + ex.Message);
                    Util.WriteToChat("Source: " + ex.Source);
                    Util.WriteToChat("Stack: " + ex.StackTrace);
                    if (ex.InnerException != null) {
                        Util.WriteToChat("Inner: " + ex.InnerException.Message);
                        Util.WriteToChat("Inner Stack: " + ex.InnerException.StackTrace);
                    }
                }
            }
            catch {
            }
        }

        internal static string GetDisplayName(WorldObject wo) {
            if (wo.Values(LongValueKey.Material, 0) > 0) {
                FileService service = Globals.Core.Filter<FileService>();
                var material = service.MaterialTable.GetById(wo.Values(LongValueKey.Material, 0));

                if (wo.ObjectClass == ObjectClass.Salvage) {
                    if (!wo.HasIdData) return $"{material} {wo.Name} ({wo.Values(LongValueKey.UsesRemaining)}) [w??]";
                    return string.Format("{0} {1} ({3}) [w{2}]", material, wo.Name, Math.Round(wo.Values(DoubleValueKey.SalvageWorkmanship) * 100) / 100, wo.Values(LongValueKey.UsesRemaining));
                }
                else {
                    if (!wo.HasIdData) return $"{material} {wo.Name} [w??t??]";
                    return string.Format("{0} {1} [w{2}t{3}]", material, wo.Name, wo.Values(LongValueKey.Workmanship), wo.Values(LongValueKey.NumberTimesTinkered));
                }
            }
            else {
                return string.Format("{0}", wo.Name);
            }
        }

        internal static string GetFullItemName(CachedWorldObject wo) {
            if (wo.Values(LongValueKey.Material, 0) > 0) {
                FileService service = Globals.Core.Filter<FileService>();
                var material = service.MaterialTable.GetById(wo.Values(LongValueKey.Material, 0));

                if (wo.ObjectClass == ObjectClass.Salvage) {
                    return string.Format("{0} {1} [w{2}]", material, wo.Name, Math.Round(wo.Values(DoubleValueKey.SalvageWorkmanship) * 100) / 100, wo.Values(LongValueKey.UsesRemaining));
                }
                else {
                    if (!wo.HasIdData) {
                        return $"{material} {wo.Name}";
                    }
                    else if (wo.Values(LongValueKey.NumberTimesTinkered, 0) > 0) {
                        return string.Format("{0} {1} [w{2}t{3}]", material, wo.Name, wo.Values(LongValueKey.Workmanship), wo.Values(LongValueKey.NumberTimesTinkered, 0));
                    }
                    else {
                        return string.Format("{0} {1} [w{2}]", material, wo.Name, wo.Values(LongValueKey.Workmanship));
                    }
                }
            }
            else {
                return string.Format("{0}", wo.Name);
            }
        }

        internal static Assembly GetAssembly() {
            return Assembly.GetExecutingAssembly();
        }

        internal static void WriteToChat(string message) {
            try {
                Globals.Host.Actions.AddChatText("<{" + Globals.PluginName + "}>: " + message, 1);
            }
            catch (Exception ex) { LogException(ex); }
        }

        internal static void ColorToHSV(System.Drawing.Color color, out double hue, out double saturation, out double value) {
            int max = Math.Max(color.R, Math.Max(color.G, color.B));
            int min = Math.Min(color.R, Math.Min(color.G, color.B));

            hue = color.GetHue();
            saturation = (max == 0) ? 0 : 1d - (1d * min / max);
            value = max / 255d;
        }
    }
}
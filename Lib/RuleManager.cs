﻿using BotShopper.Lib.Conditions;
using Decal.Adapter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BotShopper.Lib {
    class RuleEventArgs : EventArgs {
        public Rule Rule;

        public RuleEventArgs(Rule rule) {
            Rule = rule;
        }
    }

    class RuleManager {
        public event EventHandler<RuleEventArgs> RuleAdded;
        public event EventHandler<RuleEventArgs> RuleUpdated;
        public event EventHandler<RuleEventArgs> RuleRemoved;

        public List<Rule> rules = new List<Rule>();
        public static JsonSerializerSettings serializerSettings = new JsonSerializerSettings();

        public void Init() {
        }

        private static string GetRulesFile() {
            return Path.Combine(Util.GetRulesStoragePath(), $"default.json");
        }

        public static RuleManager LoadRules() {
            RuleManager manager = null;
            
            serializerSettings.TypeNameHandling = TypeNameHandling.Auto;

            if (File.Exists(GetRulesFile())) {
                try {
                    manager = JsonConvert.DeserializeObject<RuleManager>(File.ReadAllText(GetRulesFile()), serializerSettings);
                }
                catch (Exception ex) { Util.LogException(ex); }

                if (manager != null) return manager;
            }

            manager = new RuleManager();

            return manager;
        }

        public bool DoesItemNeedId() {
            foreach (var rule in rules) {
                foreach (var condition in rule.conditions) {
                    if (condition.NeedsIdData()) return true;
                }
            }

            return false;
        }

        public Rule GetMatchingRule(int id) {
            foreach (var rule in rules) {
                bool allConditionsMatch = true;

                rule.matchReasons.Clear();

                foreach (var condition in rule.conditions) {
                    if (condition.MatchesItem(ItemCache.Get(id))) {
                        if (!String.IsNullOrEmpty(condition.MatchReason)) {
                            rule.matchReasons.Add(condition.MatchReason);
                        }
                    }
                    else {
                        allConditionsMatch = false;
                        break;
                    }
                }

                if (allConditionsMatch) return rule;
            }

            return null;
        }

        internal void Save() {
            var serialized = JsonConvert.SerializeObject(this, Formatting.Indented, serializerSettings);

            File.WriteAllText(GetRulesFile(), serialized);
        }

        internal bool DoesRuleExist(Rule rule) {
            return rules.Find(r => { return r.Name == rule.Name; }) != null;
        }

        internal void AddOrUpdateRule(Rule rule) {
            var existingRuleIndex = rules.FindIndex(r => { return r == rule; });

            if (existingRuleIndex >= 0) {
                rules[existingRuleIndex] = rule;
                RuleUpdated?.Invoke(this, new RuleEventArgs(rule));
            }
            else {
                rules.Add(rule);
                RuleAdded?.Invoke(this, new RuleEventArgs(rule));
            }

            Save();
        }

        internal void RemoveRule(int existingRuleIndex) {
            var rule = rules[existingRuleIndex];
            if (existingRuleIndex >= 0) {
                rules.RemoveAt(existingRuleIndex);
                RuleRemoved?.Invoke(this, new RuleEventArgs(rule));
            }

            Save();
        }

        internal void MoveRuleUp(int existingRuleIndex) {
            if (existingRuleIndex <= 0) return;

            var rule = rules[existingRuleIndex];
            rules.RemoveAt(existingRuleIndex);
            rules.Insert(Math.Max(existingRuleIndex - 1, 0), rule);

            Save();
        }

        internal void MoveRuleDown(int existingRuleIndex) {
            if (existingRuleIndex >= rules.Count-1) return;

            var rule = rules[existingRuleIndex];
            rules.RemoveAt(existingRuleIndex);
            rules.Insert(Math.Min(existingRuleIndex + 1, rules.Count), rule);

            Save();
        }

        internal string GetNextAvailableRuleName() {
            var i = 1;
            var takenNames = rules.Select(r => { return r.Name; });

            while (true) {
                var name = $"New Rule #{i++}";

                if (!takenNames.Contains(name)) return name;
            }
        }
    }
}

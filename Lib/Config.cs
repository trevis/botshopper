﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BotShopper.Lib {
    public class Config {
        public bool HideDuplicateCommonItems = false;
        public bool AutoCheckPointValues = true;
        public bool AutoOpenPluginWindowOnTrade = false;

        private string lastConfig = "";

        internal void Save() {
            try {
                var newData = JsonConvert.SerializeObject(this, Formatting.Indented);

                if (newData == lastConfig) return;

                lastConfig = newData;

                File.WriteAllText(GetConfigPath(), newData);
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        internal static Config Load() {
            try {
                var configPath = GetConfigPath();

                if (!File.Exists(configPath)) return new Config();

                var json = File.ReadAllText(configPath);
                var config = JsonConvert.DeserializeObject<Config>(json);

                return config == null ? new Config() : config;
            }
            catch (Exception ex) { Util.LogException(ex); }

            return new Config();
        }

        private static string GetConfigPath() {
            return Path.Combine(Util.GetPluginStoragePath(), "config.json");
        }
    }
}

﻿using System;

using Decal.Adapter;
using Decal.Adapter.Wrappers;

namespace BotShopper.Lib {
	public static class Globals
	{
		public static void Init(PluginCore plugin, string pluginName, PluginHost host, CoreManager core)
		{
            Plugin = plugin;

			PluginName = pluginName;

			Host = host;

			Core = core;

            Config = Config.Load();

        }

        public static PluginCore Plugin { get; private set; }

        public static string PluginName { get; private set; }

        public static PluginHost Host { get; private set; }

        public static CoreManager Core { get; private set; }

        public static Config Config { get; private set; }
    }
}

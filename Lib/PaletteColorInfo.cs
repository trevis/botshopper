﻿using Decal.Filters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace BotShopper.Lib {
    class PaletteColorInfo {
        public int Id { get; set; }
        public string Name { get; set; }
        public string RGB { get; set; }
        public Color ExampleColor { get; set; }
        public Color Color { get; set; }

        public PaletteColorInfo(int id, string rgb) {
            Id = id;
            RGB = rgb;

            var red = byte.Parse($"{RGB[0]}{RGB[1]}", System.Globalization.NumberStyles.HexNumber);
            var green = byte.Parse($"{RGB[2]}{RGB[3]}", System.Globalization.NumberStyles.HexNumber);
            var blue = byte.Parse($"{RGB[4]}{RGB[5]}", System.Globalization.NumberStyles.HexNumber);

            var color = Color.FromArgb(255, red, green, blue);
            var colors = Enum.GetValues(typeof(KnownColor))
                .Cast<KnownColor>()
                .Select(Color.FromKnownColor);

            var closest = colors.Aggregate(Color.Black,
                            (accu, curr) =>
                            ColorDiff(color, curr) < ColorDiff(color, accu) ? curr : accu);

            Color = color;
            ExampleColor = closest;
            Name = closest.Name;
        }

        // make this better, it doesnt work with grays?
        private double ColorDiff(Color color, Color curr) {
            double h, s, v;
            double ht, st, vt;
            Util.ColorToHSV(color, out h, out s, out v);
            Util.ColorToHSV(curr, out ht, out st, out vt);

            double ss = s - st;
            double vv = v - vt;

            double svdiff =  Math.Sqrt(ss * ss + vv * vv);
            double hdiff = Math.Abs(h - ht)/255;

            return svdiff + hdiff;
        }

        public static PaletteColorInfo Get(int id, int offset, int length) {
            FileService service = Globals.Core.Filter<FileService>();
            byte[] portalFile = service.GetPortalFile(0x04000000 + id);
            var r = 0;
            var g = 0;
            var b = 0;

            var datId = BitConverter.ToInt32(portalFile, 0);
            var numColors = BitConverter.ToInt32(portalFile, 4);

            for (var i = 0; i < length; i++) {
                var pr = portalFile[8 + (offset * 4) + (i * 4) + 2];
                var pg = portalFile[8 + (offset * 4) + (i * 4) + 1];
                var pb = portalFile[8 + (offset * 4) + (i * 4) + 0];

                r += pr;
                g += pg;
                b += pb;

                //Util.WriteToChat($"{i} #{pr.ToString("X2")}{pg.ToString("X2")}{pb.ToString("X2")}");
            }

            if (length > 0) {
                r = r / length;
                g = g / length;
                b = b / length;
            }

            var rgb = $"{r.ToString("X2")}{g.ToString("X2")}{b.ToString("X2")}";
            
            return new PaletteColorInfo(id, rgb);
        }
    }
}

﻿using BotShopper.Lib.Cache;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Decal.Interop.Net;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace BotShopper.Lib {
    class TradeItemWithDataAddedEventArgs : EventArgs {
        public CachedWorldObject Added;
        public Rule MatchingRule;
        public string Reason;

        public TradeItemWithDataAddedEventArgs(CachedWorldObject wo, Rule matchingRule, string reason) {
            Added = wo;
            MatchingRule = matchingRule;
            Reason = reason;
        }
    }

    class IdQueueChangedEventArgs : EventArgs {
        public int ItemsLeftToId = 0;
        public int TotalItemsToId = 0;

        public IdQueueChangedEventArgs(int itemsLeftToId, int totalItemsToId) {
            ItemsLeftToId = itemsLeftToId;
            TotalItemsToId = totalItemsToId;
        }
    }

    class TradeManager : IDisposable {
        bool disposed = false;

        public bool IsTrading = false;

        public event EventHandler<EnterTradeEventArgs> TradeStarted;
        public event EventHandler<EndTradeEventArgs> TradeEnded;
        public event EventHandler<TradeItemWithDataAddedEventArgs> TradeItemAdded;

        public event EventHandler<IdQueueChangedEventArgs> IdQueueChanged;

        private List<int> trackedItems = new List<int>();
        private List<int> triggeredItems = new List<int>();

        private int itemsLeftToId = 0;
        private int totalItemsToId = 0;

        private List<CachedWorldObject> itemsToFire = new List<CachedWorldObject>();

        public int TradePartnerId = 0;

        public void Init() {
            IsTrading = false;
            CoreManager.Current.WorldFilter.EnterTrade += WorldFilter_EnterTrade;
            CoreManager.Current.WorldFilter.EndTrade += WorldFilter_EndTrade;
            CoreManager.Current.WorldFilter.AddTradeItem += WorldFilter_AddTradeItem;
            CoreManager.Current.WorldFilter.ChangeObject += WorldFilter_ChangeObject;
            CoreManager.Current.WorldFilter.CreateObject += WorldFilter_CreateObject;
            CoreManager.Current.EchoFilter.ServerDispatch += EchoFilter_ServerDispatch;
        }

        public string GetTradePartnerName() {
            try {
                var wo = CoreManager.Current.WorldFilter[TradePartnerId];
                if (wo == null) return null;

                return wo.Name;
            }
            catch (Exception ex) { Util.LogException(ex); }
            return null;
        }

        private void WorldFilter_CreateObject(object sender, CreateObjectEventArgs e) {
            try {
                if (!IsTrading) return;

                if (trackedItems.Contains(e.New.Id)) {
                    var cachedItem = ItemCache.Get(e.New.Id);
                    cachedItem.LoadFromWorldObject(CoreManager.Current.WorldFilter[cachedItem.Id]);
                    FireItemAddedToTrade(cachedItem);
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void WorldFilter_ChangeObject(object sender, ChangeObjectEventArgs e) {
            try {
                if (!IsTrading) return;

                if (e.Change == WorldChangeType.IdentReceived && trackedItems.Contains(e.Changed.Id)) {
                    var cachedItem = ItemCache.Get(e.Changed.Id);
                    cachedItem.LoadFromWorldObject(CoreManager.Current.WorldFilter[cachedItem.Id]);
                    IdQueueChanged?.Invoke(this, new IdQueueChangedEventArgs(--itemsLeftToId, totalItemsToId));
                    FireItemAddedToTrade(cachedItem);
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void EchoFilter_ServerDispatch(object sender, NetworkMessageEventArgs e) {
            try {
                if (!IsTrading) return;

                if (e.Message.Type == 0xF745) { // CreateObject
                    var model = e.Message.Struct("model");
                    var game = e.Message.Struct("game");
                    if (model != null && game != null) {
                        var id = e.Message.Value<int>("object");

                        var name = game.Value<string>("name");
                        var paletteCount = (int)model.Value<byte>("paletteCount");
                        var palettes = model.Struct("palettes");

                        var paletteList = new List<PaletteData>();

                        for (var i = 0; i < paletteCount; i++) {
                            MessageStruct p = (MessageStruct)palettes[i];

                            var palette = p.Value<int>("palette");
                            var offset = p.Value<int>("offset");
                            var length = p.Value<int>("length");

                            // The number of palette entries to copy. This is multiplied by 8.
                            // If it is 0, it defaults to 256 * 8.
                            if (length == 0) length = 256;
                            length *= 8;
                            offset *= 8;
                            var pData = new PaletteData(palette, offset, length);
                            
                            paletteList.Add(pData);
                        }

                        if (paletteList.Count > 0) {
                            ItemCache.Get(id).SetPalettes(paletteList);
                        }
                    }
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void WorldFilter_EnterTrade(object sender, EnterTradeEventArgs e) {
            try {
                if (e.TraderId != CoreManager.Current.CharacterFilter.Id) return;

                TradePartnerId = e.TradeeId;

                IsTrading = true;
                trackedItems.Clear();
                triggeredItems.Clear();
                itemsLeftToId = 0;
                totalItemsToId = 0;
                IdQueueChanged?.Invoke(this, new IdQueueChangedEventArgs(itemsLeftToId, totalItemsToId));
                TradeStarted?.Invoke(this, e);

                if (Globals.Config.AutoOpenPluginWindowOnTrade && !Globals.Plugin.mainView.view.Visible) {
                    Globals.Plugin.mainView.view.Visible = true;
                }

                if (Globals.Config.AutoCheckPointValues) {
                    ChatBox.Tell(TradePartnerId, "points");
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void WorldFilter_EndTrade(object sender, EndTradeEventArgs e) {
            try {
                TradePartnerId = 0;
                IsTrading = false;
                itemsLeftToId = 0;
                totalItemsToId = 0;
                CoreManager.Current.IDQueue.ClearAll();
                IdQueueChanged?.Invoke(this, new IdQueueChangedEventArgs(itemsLeftToId, totalItemsToId));
                TradeEnded?.Invoke(this, e);

                if (Globals.Config.AutoOpenPluginWindowOnTrade && Globals.Plugin.mainView.view.Visible) {
                    Globals.Plugin.mainView.view.Visible = false;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void WorldFilter_AddTradeItem(object sender, AddTradeItemEventArgs e) {
            try {
                if (e.SideId == 2 && !trackedItems.Contains(e.ItemId)) {
                    trackedItems.Add(e.ItemId);
                    var wo = ItemCache.Get(e.ItemId);
                    
                    if (wo != null && !Globals.Plugin.ruleManager.DoesItemNeedId()) {
                        FireItemAddedToTrade(wo);
                    }
                    else if (wo != null && wo.HasIdData) {
                        FireItemAddedToTrade(wo);
                    }
                    else {
                        CoreManager.Current.Actions.RequestId(e.ItemId);
                        IdQueueChanged?.Invoke(this, new IdQueueChangedEventArgs(++itemsLeftToId, ++totalItemsToId));
                    }
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void FireItemAddedToTrade(CachedWorldObject wo) {
            try {
                if (wo != null && !triggeredItems.Contains(wo.Id)) {
                    triggeredItems.Add(wo.Id);
                    var matchingRule = Globals.Plugin.ruleManager.GetMatchingRule(wo.Id);
                    var reason = "";
                    if (matchingRule != null) {
                        reason = String.Join(", ", matchingRule.matchReasons.ToArray());
                    }
                    TradeItemAdded?.Invoke(this, new TradeItemWithDataAddedEventArgs(wo, matchingRule, reason));
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        protected virtual void Dispose(bool disposing) {
            if (disposed)
                return;

            if (disposing) {
                CoreManager.Current.WorldFilter.EnterTrade -= WorldFilter_EnterTrade;
                CoreManager.Current.WorldFilter.EndTrade -= WorldFilter_EndTrade;
                CoreManager.Current.WorldFilter.AddTradeItem -= WorldFilter_AddTradeItem;
                CoreManager.Current.WorldFilter.ChangeObject -= WorldFilter_ChangeObject;
                CoreManager.Current.WorldFilter.CreateObject -= WorldFilter_CreateObject;
                CoreManager.Current.EchoFilter.ServerDispatch -= EchoFilter_ServerDispatch;
            }

            disposed = true;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BotShopper.Lib.Conditions;

namespace BotShopper.Lib {
    class Rule {
        private static int IdTracker = 1;

        public string Name = "UnknownRule";
        public int Id = 0;
        public List<Condition> conditions = new List<Condition>();

        public List<string> matchReasons = new List<string>();

        public Rule(string name) {
            Name = name;
            Id = IdTracker++;
        }

        internal void AddCondition(Condition condition) {
            conditions.Add(condition);
        }

        internal Rule Clone() {
            var rule = new Rule(Name);
            rule.Id = Id;
            
            foreach (var condition in conditions) {
                try {
                    var converted = Convert.ChangeType(condition.Clone(), condition.GetType());
                    rule.AddCondition((Condition)converted);
                }
                catch (Exception ex) { Util.LogException(ex); }
            }

            return rule;
        }
    }
}

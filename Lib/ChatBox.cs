﻿using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace BotShopper.Lib {
    static class ChatBox {
        static List<DateTime> chatMessageTimes = new List<DateTime>();
        public static Queue<string> messageQueue;
        private static DateTime lastThought = DateTime.UtcNow;
        private static DateTime lastChatCommandSentAt = DateTime.MinValue;

        public const int ChatCommandDelay = 500;

        public static void Init() {
            messageQueue = new Queue<string>();
        }

        public static void Tell(int player, string message) {
            var wo = CoreManager.Current.WorldFilter[player];
            if (wo != null) {
                Tell(wo.Name, message);
            }
        }

        public static void Tell(string player, string message) {
            Write($"/tell {player}, {message}");
        }

        public static void Say(string message) {
            Write($"/say {message}");
        }

        public static void Write(string message) {
            messageQueue.Enqueue(message);
        }

        private static void CleanChatMessageTimes() {
            var times = new List<DateTime>();

            foreach (var time in chatMessageTimes) {
                if (DateTime.UtcNow - time <= TimeSpan.FromSeconds(9)) {
                    times.Add(time);
                }
            }

            chatMessageTimes.Clear();
            chatMessageTimes.AddRange(times);
        }

        public static bool CanSendChatMessage() {
            CleanChatMessageTimes();

            return !(chatMessageTimes.Count >= 8);
        }

        public static void Think() {
            if (DateTime.UtcNow - lastChatCommandSentAt > TimeSpan.FromMilliseconds(ChatCommandDelay)) {
                if (Globals.Plugin.tradeManager.IsTrading == false) {
                    if (messageQueue.Count > 0) messageQueue.Clear();
                    return;
                }

                if (messageQueue.Count > 0 && CanSendChatMessage()) {
                    var command = messageQueue.Dequeue();

                    lastChatCommandSentAt = DateTime.UtcNow;
                    chatMessageTimes.Add(DateTime.UtcNow);
                    
                    CoreManager.Current.Actions.InvokeChatParser(command);
                }
            }
        }
    }
}

﻿using BotShopper.Lib.Cache;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using VirindiViewService.Controls;

namespace BotShopper.Lib.Conditions {
    [JsonObject(MemberSerialization.OptIn)]
    class CharacterBaseSkillCondition : Condition {
        [JsonProperty]
        public string Skill = "Alchemy";
        [JsonProperty]
        public string ComparisonType = ">=";
        [JsonProperty]
        public int Value = 50;

        HudCombo skillCombo;
        HudCombo comparisonTypeCombo;
        HudTextBox skillValue;

        public CharacterBaseSkillCondition() {
            this.Name = "Character Base Skill";
        }

        public override string ToString() {
            return $"Base {Skill} {ComparisonType} {Value}";
        }

        public override object Clone() {
            var clone = new CharacterBaseSkillCondition();

            clone.Name = Name;
            clone.Skill = Skill;
            clone.ComparisonType = ComparisonType;
            clone.Value = Value;

            return clone;
        }

        public override bool NeedsIdData() {
            return false;
        }

        public override bool MatchesItem(CachedWorldObject wo) {
            try {
                foreach (var skill in CoreManager.Current.CharacterFilter.Skills) {
                    if (skill.Name == Skill) {
                        switch (ComparisonType) {
                            case "==":
                                return skill.Base == Value;
                            case "!=":
                                return skill.Base != Value;
                            case "<=":
                                return skill.Base <= Value;
                            case ">=":
                                return skill.Base >= Value;
                        }
                    }
                }
            }
            catch (Exception ex) { Util.LogException(ex); }

            return false;
        }

        public override void DrawUI(HudFixedLayout parentLayout) {
            DrawEditConditionLayoutHeader(parentLayout, this.Name);

            skillCombo = new HudCombo(new VirindiViewService.ControlGroup());
            comparisonTypeCombo = new HudCombo(new VirindiViewService.ControlGroup());
            skillValue = new HudTextBox();


            var skills = new List<string>(CoreManager.Current.CharacterFilter.Skills.Select(s => s.Name ));
            skills.Sort();

            var i = 0;
            foreach (var skill in skills) {
                comparisonTypeCombo.AddItem(skill, skill);
                if (skill == Skill) comparisonTypeCombo.Current = i;
                i++;
            }

            i = 0;
            foreach (var type in new List<string>() { ">=", "<=", "==", "!=" }) {
                comparisonTypeCombo.AddItem(type, type);
                if (type == ComparisonType) comparisonTypeCombo.Current = i;
                i++;
            }

            skillCombo.Change += Save;
            comparisonTypeCombo.Change += Save;
            skillValue.Change += Save;

            AddControl(parentLayout, skillCombo, new Rectangle(0, 30, 140, 20));
            AddControl(parentLayout, comparisonTypeCombo, new Rectangle(150, 30, 25, 20));
            AddControl(parentLayout, skillValue, new Rectangle(185, 30, 50, 20));
        }

        private void Save(object sender, EventArgs e) {
            try {
                Skill = ((HudStaticText)skillCombo[skillCombo.Current]).Text;

                if (!int.TryParse(skillValue.Text, out Value)) {
                    skillValue.Text = Value.ToString();
                }

                ComparisonType = ((HudStaticText)comparisonTypeCombo[comparisonTypeCombo.Current]).Text;

                Globals.Plugin.mainView.filtersPage.PopulateRuleConditions();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}

﻿using BotShopper.Lib.Cache;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Decal.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using VirindiViewService.Controls;

namespace BotShopper.Lib.Conditions {
    [JsonObject(MemberSerialization.OptIn)]
    class ColorPaletteEntryExactMatchCondition : Condition {
        [JsonProperty]
        public string Palette = "00FF";
        [JsonProperty]
        public int Entry = 0;

        HudStaticText paletteLabel;
        HudTextBox paletteValue;
        HudStaticText entryLabel;
        HudTextBox entryValue;

        public ColorPaletteEntryExactMatchCondition() {
            this.Name = "Color Palette Entry Exact Match";
        }

        public override string ToString() {
            return $"Color Palette Entry #{Entry} = 0x{Palette}";
        }

        public override object Clone() {
            var clone = new ColorPaletteEntryExactMatchCondition();

            clone.Name = Name;
            clone.Palette = Palette;
            clone.Entry = Entry;

            return clone;
        }

        public override bool NeedsIdData() {
            return false;
        }

        public override bool MatchesItem(CachedWorldObject wo) {
            try {
                if (wo.Palettes.Count <= Entry) return false;

                var pal = wo.Palettes[Entry];

                int ppal = pal.Palette & 0xFFFFFF;
                int mpal = Convert.ToInt32(Palette, 16) & 0xFFFFFF;

                return (ppal == mpal);
            }
            catch (Exception ex) { Util.LogException(ex); }

            return false;
        }

        public override void DrawUI(HudFixedLayout parentLayout) {
            DrawEditConditionLayoutHeader(parentLayout, this.Name);

            paletteLabel = new HudStaticText();
            paletteValue = new HudTextBox();
            entryLabel = new HudStaticText();
            entryValue = new HudTextBox();

            paletteLabel.Text = "Palette (hex):";
            entryLabel.Text = "Palette Entry #:";

            paletteValue.Text = Palette.ToString();
            entryValue.Text = Entry.ToString(); ;

            paletteValue.Change += Save;
            entryValue.Change += Save;

            AddControl(parentLayout, paletteLabel, new Rectangle(0, 50, 150, 20));
            AddControl(parentLayout, paletteValue, new Rectangle(160, 50, 250, 20));

            AddControl(parentLayout, entryLabel, new Rectangle(0, 30, 150, 20));
            AddControl(parentLayout, entryValue, new Rectangle(160, 30, 250, 20));
        }

        private void Save(object sender, EventArgs e) {
            try {
                Palette = paletteValue.Text;

                if (!int.TryParse(entryValue.Text, out Entry)) {
                    entryValue.Text = Entry.ToString();
                }

                Globals.Plugin.mainView.filtersPage.PopulateRuleConditions();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}

﻿using BotShopper.Lib.Cache;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Decal.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using VirindiViewService.Controls;

namespace BotShopper.Lib.Conditions {
    [JsonObject(MemberSerialization.OptIn)]
    class DoubleKeyValueCondition : Condition {
        [JsonProperty]
        public DoubleValueKey Key = DoubleValueKey.AcidProt;
        [JsonProperty]
        public double Value = 0;
        [JsonProperty]
        public string ComparisonType = "==";

        HudCombo doubleValueKeyCombo;
        HudCombo comparisonTypeCombo;
        HudTextBox valueTextBox;

        HudFixedLayout _parentLayout;

        FileService fileService = Globals.Core.Filter<FileService>();

        public static List<DoubleValueKey> KeysThatDontNeedIdData = new List<DoubleValueKey>() {
            DoubleValueKey.ApproachDistance,
            DoubleValueKey.SalvageWorkmanship
        };

        public DoubleKeyValueCondition() {
            this.Name = "Double Key Value";
        }

        private void KeyCombo_Change(object sender, EventArgs e) {
            try {
                DoubleValueKey key = (DoubleValueKey)Enum.Parse(typeof(DoubleValueKey), ((HudStaticText)doubleValueKeyCombo[doubleValueKeyCombo.Current]).Text);
                if ((int)key != 0) {
                    Key = key;
                    Globals.Plugin.ruleManager.Save();
                }

            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void ValueTextBox_LostFocus(object sender, EventArgs e) {
            try {
                if (int.TryParse(valueTextBox.Text, out int Value)) {
                    Globals.Plugin.ruleManager.Save();
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        public override object Clone() {
            var clone = new DoubleKeyValueCondition();

            clone.Name = Name;
            clone.Key = Key;
            clone.Value = Value;
            clone.ComparisonType = ComparisonType;

            return clone;
        }

        public override string ToString() {
            return $"{Key} {ComparisonType} {Value}";
        }

        public override bool NeedsIdData() {
            return !KeysThatDontNeedIdData.Contains(Key);
        }

        public override bool MatchesItem(CachedWorldObject wo) {
            var isMatch = false;

            if (wo == null) return false;

            switch (ComparisonType) {
                case ">=":
                    isMatch = wo.Values(Key, -1) >= Value;
                    break;

                case "<=":
                    isMatch = wo.Values(Key, -1) <= Value;
                    break;

                case "!=":
                    isMatch = wo.Values(Key, -1) != Value;
                    break;

                default:
                    isMatch = wo.Values(Key, -1) == Value;
                    break;
            }

            if (isMatch) MatchReason = ToString();

            return isMatch;
        }

        public override void DrawUI(HudFixedLayout parentLayout) {
            _parentLayout = parentLayout;
            DrawEditConditionLayoutHeader(parentLayout, this.Name);

            doubleValueKeyCombo = new HudCombo(new VirindiViewService.ControlGroup());
            comparisonTypeCombo = new HudCombo(new VirindiViewService.ControlGroup());

            var doubleKeyValues = Enum.GetNames(typeof(DoubleValueKey));
            Array.Sort(doubleKeyValues);

            var i = 0;
            foreach (var e in doubleKeyValues) {
                doubleValueKeyCombo.AddItem(e, e);
                if (Key == (DoubleValueKey)Enum.Parse(typeof(DoubleValueKey), e)) {
                    doubleValueKeyCombo.Current = i;
                }
                i++;
            }

            i = 0;
            foreach (var type in new List<string>() { ">=", "<=", "==", "!=" }) {
                comparisonTypeCombo.AddItem(type, type);
                if (type == ComparisonType) comparisonTypeCombo.Current = i;
                i++;
            }

            doubleValueKeyCombo.Change += LongValueKeyCombo_Change; ;
            comparisonTypeCombo.Change += Save;

            AddControl(parentLayout, doubleValueKeyCombo, new Rectangle(0, 30, 150, 20));
            AddControl(parentLayout, comparisonTypeCombo, new Rectangle(160, 30, 50, 20));
            
            valueTextBox = new HudTextBox();
            valueTextBox.Text = Value.ToString();
            valueTextBox.Change += Save;
            AddControl(parentLayout, valueTextBox, new Rectangle(220, 30, 100, 20));
        }

        private void LongValueKeyCombo_Change(object sender, EventArgs e) {
            try {
                Save(sender, e);
                if (_parentLayout != null) DrawUI(_parentLayout);
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void Save(object sender, EventArgs e) {
            try {
                Key = (DoubleValueKey)Enum.Parse(typeof(DoubleValueKey), ((HudStaticText)doubleValueKeyCombo[doubleValueKeyCombo.Current]).Text);
                ComparisonType = ((HudStaticText)comparisonTypeCombo[comparisonTypeCombo.Current]).Text;
                
                if (!double.TryParse(valueTextBox.Text, out Value)) {
                    Util.WriteToChat($"Error converting '{valueTextBox.Text}' to double");
                    valueTextBox.Text = Value.ToString();
                }

                Globals.Plugin.mainView.filtersPage.PopulateRuleConditions();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}

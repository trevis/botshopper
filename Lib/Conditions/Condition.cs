﻿using BotShopper.Lib.Cache;
using BotShopper.Lib.Conditions;
using Decal.Adapter.Wrappers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using VirindiViewService.Controls;

namespace BotShopper.Lib {
    abstract class Condition {
        public string Name = "UnknownCondition";
        public string MatchReason = "";

        override abstract public string ToString();
        abstract public object Clone();
        abstract public bool NeedsIdData();
        abstract public bool MatchesItem(CachedWorldObject item);
        abstract public void DrawUI(HudFixedLayout parentLayout);

        public static List<HudControl> hudControls = new List<HudControl>();

        public static HudStaticText UIFiltersConditionTypeLabel;
        public static HudCombo UIFiltersConditionType;
        public static HudButton UIFiltersSaveCondition;
        static List<Condition> availableConditions = new List<Condition>();
        static Dictionary<string, Condition> availableConditionsByName = new Dictionary<string, Condition>();

        public bool MatchesItemId(int id) {
            return MatchesItem(ItemCache.Get(id));
        }

        public static void AddControl(HudFixedLayout layout, HudControl control, Rectangle rect) {
            hudControls.Add(control);
            layout.AddControl(control, rect);
        }

        public static void ResetControls(HudFixedLayout layout) {
            ClearControls(layout);
        }

        public static void ClearControls(HudFixedLayout layout) {
            foreach (var control in hudControls) {
                layout.RemovedChild(control);
            }

            hudControls.Clear();

            if (UIFiltersSaveCondition != null) layout.RemovedChild(UIFiltersSaveCondition);
            if (UIFiltersConditionType != null) layout.RemovedChild(UIFiltersConditionType);
            if (UIFiltersConditionTypeLabel != null) layout.RemovedChild(UIFiltersConditionTypeLabel);

        }

        public static void DrawEditConditionLayoutHeader(HudFixedLayout UIFiltersConditionLayout, string selected) {
            ClearControls(UIFiltersConditionLayout);

            UIFiltersSaveCondition = new HudButton();
            UIFiltersConditionType = new HudCombo(new VirindiViewService.ControlGroup());
            UIFiltersConditionTypeLabel = new HudStaticText();

            UIFiltersSaveCondition.Text = "Save Condition";
            UIFiltersConditionTypeLabel.Text = "Condition type:";

            availableConditions.Clear();
            UIFiltersConditionType.Clear();

            var conditions = new List<Condition>() {
                new AnySimilarColorCondition(),
                new BuffedDoubleValueKeyCondition(),
                new BuffedLongValueKeyCondition(),
                new BuffedMedianDamageCondition(),
                new BuffedMissileDamageCondition(),
                new CalcedBuffedTinkedDamageCondition(),
                new CalcedBuffedTinkedTargetWeaponModCondition(),
                new CharacterBaseSkillCondition(),
                new CharacterBuffedSkillCondition(),
                new CharacterLevelCondition(),
                new DoubleKeyValueCondition(),
                new FreeMainPackSpaceCondition(),
                new LongKeyValueCondition(),
                new MinimumDamageCondition(),
                new ObjectClassCondition(),
                new ColorPaletteEntryExactMatchCondition(),
                new SpellMatchCondition(),
                new StringKeyValueMatchCondition(),
                new TotalRatingsCondition(),
                new VTankLootProfileCondition()
            };

            foreach (var condition in conditions) {
                availableConditions.Add(condition);
                if (!availableConditionsByName.ContainsKey(condition.Name)) {
                    availableConditionsByName.Add(condition.Name, condition);
                }
            }

            var i = 0;
            foreach (var condition in availableConditions) {
                UIFiltersConditionType.AddItem(condition.Name, condition.Name);

                if (condition.Name == selected) UIFiltersConditionType.Current = i;

                i++;
            }

            UIFiltersSaveCondition.Hit += UIFiltersSaveCondition_Hit;
            UIFiltersConditionType.Change += UIFiltersConditionType_Change;

            Condition.AddControl(UIFiltersConditionLayout, UIFiltersConditionTypeLabel, new System.Drawing.Rectangle(0, 0, 80, 19));
            Condition.AddControl(UIFiltersConditionLayout, UIFiltersConditionType, new System.Drawing.Rectangle(90, 0, 216, 19));
            //Condition.AddControl(UIFiltersConditionLayout, UIFiltersSaveCondition, new System.Drawing.Rectangle(570, 158, 96, 16));
        }

        private static void UIFiltersConditionType_Change(object sender, EventArgs e) {
            try {
                var conditionType = ((HudStaticText)UIFiltersConditionType[UIFiltersConditionType.Current]).Text;

                if (availableConditionsByName.ContainsKey(conditionType)) {
                    var condition = availableConditionsByName[conditionType];

                    Globals.Plugin.mainView.filtersPage.ReplaceSelectedCondition(condition);

                    condition.DrawUI(Globals.Plugin.mainView.filtersPage.UIFiltersConditionLayout);
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private static void UIFiltersSaveCondition_Hit(object sender, EventArgs e) {
            try {
                Util.WriteToChat("Save condition for rule: " + Globals.Plugin.mainView.filtersPage.UIFiltersRuleName.Text);
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}

﻿using BotShopper.Lib.Cache;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using VirindiViewService.Controls;

namespace BotShopper.Lib.Conditions {
    [JsonObject(MemberSerialization.OptIn)]
    class CharacterLevelCondition : Condition {
        [JsonProperty]
        public string ComparisonType = ">=";
        [JsonProperty]
        public int Value = 50;

        HudCombo comparisonTypeCombo;
        HudStaticText characterLevelLabel;
        HudTextBox characterLevelValue;

        public CharacterLevelCondition() {
            this.Name = "Character Level";
        }

        public override string ToString() {
            return $"Character Level {ComparisonType} {Value}";
        }

        public override object Clone() {
            var clone = new TotalRatingsCondition();

            clone.Name = Name;
            clone.ComparisonType = ComparisonType;
            clone.Value = Value;

            return clone;
        }

        public override bool NeedsIdData() {
            return false;
        }

        public override bool MatchesItem(CachedWorldObject wo) {
            try {
                var characterLevel = CoreManager.Current.CharacterFilter.Level;

                switch (ComparisonType) {
                    case "==":
                        return characterLevel == Value;
                    case "!=":
                        return characterLevel != Value;
                    case "<=":
                        return characterLevel <= Value;
                    default:
                        return characterLevel >= Value;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }

            return false;
        }

        public override void DrawUI(HudFixedLayout parentLayout) {
            DrawEditConditionLayoutHeader(parentLayout, this.Name);

            comparisonTypeCombo = new HudCombo(new VirindiViewService.ControlGroup());
            characterLevelLabel = new HudStaticText();
            characterLevelValue = new HudTextBox();


            var objectClasses = Enum.GetNames(typeof(ObjectClass));
            Array.Sort(objectClasses);

            var i = 0;
            foreach (var type in new List<string>() { ">=", "<=", "==", "!=" }) {
                comparisonTypeCombo.AddItem(type, type);
                if (type == ComparisonType) comparisonTypeCombo.Current = i;
                i++;
            }

            characterLevelLabel.Text = "Character Level";

            comparisonTypeCombo.Change += Save;
            characterLevelValue.Change += Save;

            AddControl(parentLayout, characterLevelLabel, new Rectangle(0, 30, 140, 20));
            AddControl(parentLayout, comparisonTypeCombo, new Rectangle(150, 30, 25, 20));
            AddControl(parentLayout, characterLevelValue, new Rectangle(185, 30, 50, 20));
        }

        private void Save(object sender, EventArgs e) {
            try {
                if (!int.TryParse(characterLevelValue.Text, out Value)) {
                    characterLevelValue.Text = Value.ToString();
                }

                ComparisonType = ((HudStaticText)comparisonTypeCombo[comparisonTypeCombo.Current]).Text;

                Globals.Plugin.mainView.filtersPage.PopulateRuleConditions();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}

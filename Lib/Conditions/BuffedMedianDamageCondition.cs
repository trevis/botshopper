﻿using BotShopper.Lib.Cache;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using VirindiViewService.Controls;

namespace BotShopper.Lib.Conditions {
    [JsonObject(MemberSerialization.OptIn)]
    class BuffedMedianDamageCondition : Condition {
        [JsonProperty]
        public string ComparisonType = ">=";
        [JsonProperty]
        public double Value = 20;

        HudCombo comparisonTypeCombo;
        HudStaticText buffedMedianDamageLabel;
        HudTextBox buffedMedianDamageValue;

        public BuffedMedianDamageCondition() {
            this.Name = "Buffed Median Damage";
        }

        public override string ToString() {
            return $"Buffed Median Damage {ComparisonType} {Value}";
        }

        public override object Clone() {
            var clone = new TotalRatingsCondition();

            clone.Name = Name;
            clone.ComparisonType = ComparisonType;
            clone.Value = Value;

            return clone;
        }

        public override bool NeedsIdData() {
            return false;
        }

        public override bool MatchesItem(CachedWorldObject wo) {
            try {
                int maxdamage = wo.Values(LongValueKey.MaxDamage);

                double variance = wo.Values(DoubleValueKey.Variance, 0.0);
                double mindamage = maxdamage - (variance * maxdamage);
                double damage = (mindamage + maxdamage) / 2;

                switch (ComparisonType) {
                    case "==":
                        return damage == Value;
                    case "!=":
                        return damage != Value;
                    case "<=":
                        return damage <= Value;
                    case ">=":
                        return damage >= Value;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }

            return false;
        }

        public override void DrawUI(HudFixedLayout parentLayout) {
            DrawEditConditionLayoutHeader(parentLayout, this.Name);

            comparisonTypeCombo = new HudCombo(new VirindiViewService.ControlGroup());
            buffedMedianDamageLabel = new HudStaticText();
            buffedMedianDamageValue = new HudTextBox();


            var objectClasses = Enum.GetNames(typeof(ObjectClass));
            Array.Sort(objectClasses);

            var i = 0;
            foreach (var type in new List<string>() { ">=", "<=", "==", "!=" }) {
                comparisonTypeCombo.AddItem(type, type);
                if (type == ComparisonType) comparisonTypeCombo.Current = i;
                i++;
            }

            buffedMedianDamageLabel.Text = "Buffed Median Damage";

            comparisonTypeCombo.Change += Save;
            buffedMedianDamageValue.Change += Save;

            AddControl(parentLayout, buffedMedianDamageLabel, new Rectangle(0, 30, 140, 20));
            AddControl(parentLayout, comparisonTypeCombo, new Rectangle(150, 30, 25, 20));
            AddControl(parentLayout, buffedMedianDamageValue, new Rectangle(185, 30, 50, 20));
        }

        private void Save(object sender, EventArgs e) {
            try {
                if (!double.TryParse(buffedMedianDamageValue.Text, out Value)) {
                    buffedMedianDamageValue.Text = Value.ToString();
                }

                ComparisonType = ((HudStaticText)comparisonTypeCombo[comparisonTypeCombo.Current]).Text;

                Globals.Plugin.mainView.filtersPage.PopulateRuleConditions();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}

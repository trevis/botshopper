﻿using BotShopper.Lib.Cache;
using Decal.Adapter;
using Newtonsoft.Json;
using System;

namespace BotShopper.Lib.Conditions {
    [JsonObject(MemberSerialization.OptIn)]
    class CharacterBuffedSkillCondition : CharacterBaseSkillCondition {

        public CharacterBuffedSkillCondition() {
            this.Name = "Character Buffed Skill";
        }

        public override string ToString() {
            return $"Buffed {Skill} {ComparisonType} {Value}";
        }

        public override bool MatchesItem(CachedWorldObject wo) {
            try {
                foreach (var skill in CoreManager.Current.CharacterFilter.Skills) {
                    if (skill.Name == Skill) {
                        switch (ComparisonType) {
                            case "==":
                                return skill.Buffed == Value;
                            case "!=":
                                return skill.Buffed != Value;
                            case "<=":
                                return skill.Buffed <= Value;
                            case ">=":
                                return skill.Buffed >= Value;
                        }
                    }
                }
            }
            catch (Exception ex) { Util.LogException(ex); }

            return false;
        }
    }
}

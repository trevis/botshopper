﻿using BotShopper.Lib.Cache;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using VirindiViewService.Controls;

namespace BotShopper.Lib.Conditions {
    [JsonObject(MemberSerialization.OptIn)]
    class ObjectClassCondition : Condition {
        [JsonProperty]
        public ObjectClass ObjectClass = ObjectClass.Unknown;

        HudCombo objectClassCombo;
        HudStaticText objectClassLabel;

        public ObjectClassCondition() {
            this.Name = "Object Class";
        }

        public override string ToString() {
            return $"ObjectClass = {ObjectClass.ToString()}";
        }

        public override object Clone() {
            var clone = new ObjectClassCondition();

            clone.Name = Name;
            clone.ObjectClass = ObjectClass;

            return clone;
        }

        public override bool NeedsIdData() {
            return false;
        }

        public override bool MatchesItem(CachedWorldObject wo) {
            if (wo == null) return false;

            if (wo.ObjectClass == ObjectClass) {
                MatchReason = ToString();
                return true;
            }

            return false;
        }

        public override void DrawUI(HudFixedLayout parentLayout) {
            DrawEditConditionLayoutHeader(parentLayout, Name);

            objectClassCombo = new HudCombo(new VirindiViewService.ControlGroup());
            objectClassLabel = new HudStaticText();

            var objectClasses = Enum.GetNames(typeof(ObjectClass));
            Array.Sort(objectClasses);

            var i = 0;
            foreach (var objectClass in objectClasses) {
                objectClassCombo.AddItem(objectClass, objectClass);
                if (ObjectClass == (ObjectClass)Enum.Parse(typeof(ObjectClass), objectClass)) {
                    objectClassCombo.Current = i;
                }
                i++;
            }

            objectClassLabel.Text = "ObjectClass == ";

            objectClassCombo.Change += ObjectClassCombo_Change;

            AddControl(parentLayout, objectClassLabel, new Rectangle(0, 30, 80, 20));
            AddControl(parentLayout, objectClassCombo, new Rectangle(80, 30, 100, 20));
        }

        private void ObjectClassCombo_Change(object sender, EventArgs e) {
            try {
                ObjectClass = (ObjectClass)Enum.Parse(typeof(ObjectClass), ((HudStaticText)objectClassCombo[objectClassCombo.Current]).Text);

                Globals.Plugin.mainView.filtersPage.PopulateRuleConditions();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}

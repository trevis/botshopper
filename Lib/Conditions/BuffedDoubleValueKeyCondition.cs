﻿using BotShopper.Lib.Cache;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using VirindiViewService.Controls;

namespace BotShopper.Lib.Conditions {
    [JsonObject(MemberSerialization.OptIn)]
    class BuffedDoubleValueKeyCondition : DoubleKeyValueCondition {
        public BuffedDoubleValueKeyCondition() {
            this.Name = "Buffed Double Key Value";
        }

        public override string ToString() {
            return $"Buffed {Key} {ComparisonType} {Value}";
        }

        public override bool MatchesItem(CachedWorldObject wo) {
            try {
                double buffedValue = wo.GetBuffedDoubleValueKey(Key);

                switch (ComparisonType) {
                    case "==":
                        return buffedValue == Value;
                    case "!=":
                        return buffedValue != Value;
                    case "<=":
                        return buffedValue <= Value;
                    case ">=":
                        return buffedValue >= Value;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }

            return false;
        }
    }
}

﻿using BotShopper.Lib.Cache;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using VirindiViewService.Controls;

namespace BotShopper.Lib.Conditions {
    [JsonObject(MemberSerialization.OptIn)]
    class BuffedLongValueKeyCondition : LongKeyValueCondition {
        public BuffedLongValueKeyCondition() {
            this.Name = "Buffed Long Key Value";
        }
        
        public override string ToString() {
            switch (Key) {
                case LongValueKey.Material:
                    if (Value == 0) {
                        return $"Buffed {Key} {ComparisonType} {Value}";
                    }
                    else {
                        var material = fileService.MaterialTable.GetById(Value);
                        var materialName = material == null ? Value.ToString() : material.Name;
                        return $"Buffed {Key} {ComparisonType} {materialName}";
                    }
                default:
                    return $"{Key} {ComparisonType} {Value}";
            }
        }

        public override bool MatchesItem(CachedWorldObject wo) {
            try {
                int buffedValue = wo.GetBuffedLongValueKey(Key);

                switch (ComparisonType) {
                    case "==":
                        return buffedValue == Value;
                    case "!=":
                        return buffedValue != Value;
                    case "<=":
                        return buffedValue <= Value;
                    case ">=":
                        return buffedValue >= Value;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }

            return false;
        }
    }
}

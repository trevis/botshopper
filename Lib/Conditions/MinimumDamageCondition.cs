﻿using BotShopper.Lib.Cache;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using VirindiViewService.Controls;

namespace BotShopper.Lib.Conditions {
    [JsonObject(MemberSerialization.OptIn)]
    class MinimumDamageCondition : Condition {
        [JsonProperty]
        public string ComparisonType = ">=";
        [JsonProperty]
        public double Value = 10;

        HudCombo comparisonTypeCombo;
        HudStaticText minimumDamageLabel;
        HudTextBox minimumDamageValue;

        public MinimumDamageCondition() {
            this.Name = "Minimum Damage";
        }

        public override string ToString() {
            return $"Minimum Damage {ComparisonType} {Value}";
        }

        public override object Clone() {
            var clone = new TotalRatingsCondition();

            clone.Name = Name;
            clone.ComparisonType = ComparisonType;
            clone.Value = Value;

            return clone;
        }

        public override bool NeedsIdData() {
            return false;
        }

        public override bool MatchesItem(CachedWorldObject wo) {
            try {
                int maxdamage = wo.Values(LongValueKey.MaxDamage, 0);
                var minimumDamage = maxdamage - (wo.Values(DoubleValueKey.Variance, 0.0) * maxdamage);

                switch (ComparisonType) {
                    case "==":
                        return minimumDamage == Value;
                    case "!=":
                        return minimumDamage != Value;
                    case "<=":
                        return minimumDamage <= Value;
                    case ">=":
                        return minimumDamage >= Value;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }

            return false;
        }

        public override void DrawUI(HudFixedLayout parentLayout) {
            DrawEditConditionLayoutHeader(parentLayout, this.Name);

            comparisonTypeCombo = new HudCombo(new VirindiViewService.ControlGroup());
            minimumDamageLabel = new HudStaticText();
            minimumDamageValue = new HudTextBox();


            var objectClasses = Enum.GetNames(typeof(ObjectClass));
            Array.Sort(objectClasses);

            var i = 0;
            foreach (var type in new List<string>() { ">=", "<=", "==", "!=" }) {
                comparisonTypeCombo.AddItem(type, type);
                if (type == ComparisonType) comparisonTypeCombo.Current = i;
                i++;
            }

            minimumDamageLabel.Text = "Minimum Damage";

            comparisonTypeCombo.Change += Save;
            minimumDamageValue.Change += Save;

            AddControl(parentLayout, minimumDamageLabel, new Rectangle(0, 30, 140, 20));
            AddControl(parentLayout, comparisonTypeCombo, new Rectangle(150, 30, 25, 20));
            AddControl(parentLayout, minimumDamageValue, new Rectangle(185, 30, 50, 20));
        }

        private void Save(object sender, EventArgs e) {
            try {
                if (!double.TryParse(minimumDamageValue.Text, out Value)) {
                    minimumDamageValue.Text = Value.ToString();
                }

                ComparisonType = ((HudStaticText)comparisonTypeCombo[comparisonTypeCombo.Current]).Text;

                Globals.Plugin.mainView.filtersPage.PopulateRuleConditions();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}

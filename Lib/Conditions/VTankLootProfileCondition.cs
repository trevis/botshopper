﻿using BotShopper.Lib.Cache;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using VirindiViewService.Controls;
using VTClassic;

namespace BotShopper.Lib.Conditions {
    class VTLPRule {
        public string Name { get; set; }
        public List<Condition> Conditions { get; set; }

        public VTLPRule(string name, List<Condition> conditions) {
            Name = name;
            Conditions = conditions;
        }
    }

    [JsonObject(MemberSerialization.OptIn)]
    class VTankLootProfileCondition : Condition {
        [JsonProperty]
        public string LootProfile = "";

        private string loadedProfile = "";

        HudCombo profileSelectCombo = new HudCombo(new VirindiViewService.ControlGroup());
        HudStaticText profileSelectLabel = new HudStaticText();

        public List<VTLPRule> rules = new List<VTLPRule>();

        public VTankLootProfileCondition() {
            this.Name = "VTank Loot Profile";
        }

        public override string ToString() {
            return $"Matches a rule from {LootProfile}";
        }

        public override object Clone() {
            var clone = new VTankLootProfileCondition();

            clone.Name = Name;
            clone.LootProfile = LootProfile;

            return clone;
        }

        public void ParseLoadedProfile() {
            try {
                if (loadedProfile == LootProfile) return;
                loadedProfile = LootProfile;

                rules.Clear();

                var LootRules = new cLootRules();

                var profilePath = Path.Combine(Util.GetLootProfilesStoragePath(), LootProfile);

                if (!File.Exists(profilePath)) {
                    Util.WriteToChat("Unable to load profile: " + profilePath);
                    return;
                }

                StreamReader pf = new System.IO.StreamReader(profilePath);
                LootRules.Read(pf, 0);
                pf.Close();

                var conditionCount = 0;

                var i = 0;
                foreach (cLootItemRule ir in LootRules.Rules) {
                    var conditionsForRule = new List<Condition>();
                    var isDisabled = false;
                    foreach (var rule in ir.IntRules) {
                        try {
                            Condition condition = null;
                            switch (rule.GetRuleType()) {
                                case eLootRuleType.AnySimilarColor:
                                    condition = new AnySimilarColorCondition();
                                    var c = ((AnySimilarColor)rule).EColor;
                                    var hex = "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
                                    ((AnySimilarColorCondition)condition).HexColor = hex;
                                    ((AnySimilarColorCondition)condition).MaxHueDifference = (int)((AnySimilarColor)rule).MaxDifferenceH;
                                    ((AnySimilarColorCondition)condition).MaxSVDifference = ((AnySimilarColor)rule).MaxDifferenceSV;
                                    break;

                                case eLootRuleType.BuffedDoubleValKeyGE:
                                    condition = new BuffedDoubleValueKeyCondition();
                                    ((BuffedDoubleValueKeyCondition)condition).Key = (DoubleValueKey)((BuffedDoubleValKeyGE)rule).vk;
                                    ((BuffedDoubleValueKeyCondition)condition).ComparisonType = ">=";
                                    ((BuffedDoubleValueKeyCondition)condition).Value = (int)((BuffedDoubleValKeyGE)rule).keyval;
                                    break;

                                case eLootRuleType.BuffedLongValKeyGE:
                                    condition = new BuffedLongValueKeyCondition();
                                    ((BuffedLongValueKeyCondition)condition).Key = (LongValueKey)((BuffedLongValKeyGE)rule).vk;
                                    ((BuffedLongValueKeyCondition)condition).ComparisonType = ">=";
                                    ((BuffedLongValueKeyCondition)condition).Value = (int)((BuffedLongValKeyGE)rule).keyval;
                                    break;

                                case eLootRuleType.BuffedMedianDamageGE:
                                    condition = new BuffedMedianDamageCondition();
                                    ((BuffedMedianDamageCondition)condition).ComparisonType = ">=";
                                    ((BuffedMedianDamageCondition)condition).Value = ((BuffedMedianDamageGE)rule).keyval;
                                    break;

                                case eLootRuleType.BuffedMissileDamageGE:
                                    condition = new BuffedMissileDamageCondition();
                                    ((BuffedMissileDamageCondition)condition).ComparisonType = ">=";
                                    ((BuffedMissileDamageCondition)condition).Value = ((BuffedMissileDamageGE)rule).keyval;
                                    break;

                                case eLootRuleType.CalcdBuffedTinkedDamageGE:
                                    condition = new CalcedBuffedTinkedDamageCondition();
                                    ((CalcedBuffedTinkedDamageCondition)condition).ComparisonType = ">=";
                                    ((CalcedBuffedTinkedDamageCondition)condition).Value = ((CalcdBuffedTinkedDamageGE)rule).keyval;
                                    break;

                                case eLootRuleType.CalcedBuffedTinkedTargetMeleeGE:
                                    condition = new CalcedBuffedTinkedTargetWeaponModCondition();
                                    ((CalcedBuffedTinkedTargetWeaponModCondition)condition).TargetCalcedBuffedDoT = ((CalcedBuffedTinkedTargetMeleeGE)rule).targetCalcedBuffedTinkedDoT;
                                    ((CalcedBuffedTinkedTargetWeaponModCondition)condition).TargetBuffedMelee = ((CalcedBuffedTinkedTargetMeleeGE)rule).targetBuffedMeleeDefenseBonus;
                                    ((CalcedBuffedTinkedTargetWeaponModCondition)condition).TargetBuffedAttackBonus = ((CalcedBuffedTinkedTargetMeleeGE)rule).targetBuffedAttackBonus;
                                    break;

                                case eLootRuleType.CharacterBaseSkill:
                                    condition = new CharacterBaseSkillCondition();
                                    ((CharacterBaseSkillCondition)condition).ComparisonType = "<=";
                                    ((CharacterBaseSkillCondition)condition).Skill = CamelAddSpace(((CharacterBaseSkill)rule).vk.ToString());
                                    ((CharacterBaseSkillCondition)condition).Value = ((CharacterBaseSkill)rule).maxskill;
                                    conditionsForRule.Add(condition);
                                    condition = new CharacterBaseSkillCondition();
                                    ((CharacterBaseSkillCondition)condition).ComparisonType = ">=";
                                    ((CharacterBaseSkillCondition)condition).Skill = CamelAddSpace(((CharacterBaseSkill)rule).vk.ToString());
                                    ((CharacterBaseSkillCondition)condition).Value = ((CharacterBaseSkill)rule).minskill;
                                    break;

                                case eLootRuleType.CharacterLevelGE:
                                    condition = new CharacterLevelCondition();
                                    ((CharacterLevelCondition)condition).ComparisonType = ">=";
                                    ((CharacterLevelCondition)condition).Value = ((CharacterLevelGE)rule).keyval;
                                    break;

                                case eLootRuleType.CharacterLevelLE:
                                    condition = new CharacterLevelCondition();
                                    ((CharacterLevelCondition)condition).ComparisonType = "<=";
                                    ((CharacterLevelCondition)condition).Value = ((CharacterLevelLE)rule).keyval;
                                    break;
                                    
                                case eLootRuleType.CharacterMainPackEmptySlotsGE:
                                    condition = new FreeMainPackSpaceCondition();
                                    ((FreeMainPackSpaceCondition)condition).ComparisonType = ">=";
                                    ((FreeMainPackSpaceCondition)condition).Value = ((CharacterMainPackEmptySlotsGE)rule).keyval;
                                    break;

                                case eLootRuleType.CharacterSkillGE:
                                    condition = new CharacterBuffedSkillCondition();
                                    ((CharacterBuffedSkillCondition)condition).ComparisonType = ">=";
                                    ((CharacterBuffedSkillCondition)condition).Skill = CamelAddSpace(((CharacterSkillGE)rule).vk.ToString());
                                    ((CharacterBuffedSkillCondition)condition).Value = ((CharacterSkillGE)rule).keyval;
                                    break;

                                case eLootRuleType.DamagePercentGE:
                                    // rule not supported?
                                    break;
                                    

                                case eLootRuleType.DisabledRule:
                                    if (((DisabledRule)rule).b) isDisabled = true;
                                    break;
                                    
                                case eLootRuleType.DoubleValKeyGE:
                                    condition = new DoubleKeyValueCondition();
                                    ((DoubleKeyValueCondition)condition).Key = (DoubleValueKey)((DoubleValKeyGE)rule).vk;
                                    ((DoubleKeyValueCondition)condition).ComparisonType = ">=";
                                    ((DoubleKeyValueCondition)condition).Value = ((DoubleValKeyGE)rule).keyval;
                                    break;

                                case eLootRuleType.DoubleValKeyLE:
                                    condition = new DoubleKeyValueCondition();
                                    ((DoubleKeyValueCondition)condition).Key = (DoubleValueKey)((DoubleValKeyLE)rule).vk;
                                    ((DoubleKeyValueCondition)condition).ComparisonType = "<=";
                                    ((DoubleKeyValueCondition)condition).Value = ((DoubleValKeyLE)rule).keyval;
                                    break;

                                case eLootRuleType.LongValKeyE:
                                    condition = new LongKeyValueCondition();
                                    ((LongKeyValueCondition)condition).Key = (LongValueKey)((LongValKeyE)rule).vk;
                                    ((LongKeyValueCondition)condition).ComparisonType = "==";
                                    ((LongKeyValueCondition)condition).Value = ((LongValKeyE)rule).keyval;
                                    break;

                                case eLootRuleType.LongValKeyFlagExists:
                                    break;

                                case eLootRuleType.LongValKeyGE:
                                    condition = new LongKeyValueCondition();
                                    ((LongKeyValueCondition)condition).Key = (LongValueKey)((int)((LongValKeyGE)rule).vk);
                                    ((LongKeyValueCondition)condition).ComparisonType = ">=";
                                    ((LongKeyValueCondition)condition).Value = ((LongValKeyGE)rule).keyval;
                                    break;

                                case eLootRuleType.LongValKeyLE:
                                    condition = new LongKeyValueCondition();
                                    ((LongKeyValueCondition)condition).Key = (LongValueKey)((LongValKeyLE)rule).vk;
                                    ((LongKeyValueCondition)condition).ComparisonType = "<=";
                                    ((LongKeyValueCondition)condition).Value = ((LongValKeyLE)rule).keyval;
                                    break;

                                case eLootRuleType.LongValKeyNE:
                                    condition = new LongKeyValueCondition();
                                    ((LongKeyValueCondition)condition).Key = (LongValueKey)((LongValKeyNE)rule).vk;
                                    ((LongKeyValueCondition)condition).ComparisonType = "!=";
                                    ((LongKeyValueCondition)condition).Value = ((LongValKeyNE)rule).keyval;
                                    break;
                                    
                                case eLootRuleType.MinDamageGE:
                                    condition = new MinimumDamageCondition();
                                    ((MinimumDamageCondition)condition).ComparisonType = ">=";
                                    ((MinimumDamageCondition)condition).Value = ((MinDamageGE)rule).keyval;
                                    break;

                                case eLootRuleType.ObjectClass:
                                    condition = new ObjectClassCondition();
                                    ((ObjectClassCondition)condition).ObjectClass = (ObjectClass)((ObjectClassE)rule).vk;
                                    break;

                                case eLootRuleType.SimilarColorArmorType:
                                    break;

                                case eLootRuleType.SlotExactPalette:
                                    condition = new ColorPaletteEntryExactMatchCondition();
                                    ((ColorPaletteEntryExactMatchCondition)condition).Palette = ((SlotExactPalette)rule).Palette.ToString("X4");
                                    ((ColorPaletteEntryExactMatchCondition)condition).Entry = (int)((SlotExactPalette)rule).Slot;
                                    break;

                                case eLootRuleType.SlotSimilarColor:
                                    break;
                                
                                case eLootRuleType.SpellCountGE:
                                    condition = new SpellMatchCondition();
                                    ((SpellMatchCondition)condition).MinimumCount = ((SpellCountGE)rule).keyval;
                                    ((SpellMatchCondition)condition).DoesMatchPattern = "";
                                    ((SpellMatchCondition)condition).DoesNotMatchPattern = "";
                                    break;

                                case eLootRuleType.SpellMatch:
                                    condition = new SpellMatchCondition();
                                    ((SpellMatchCondition)condition).MinimumCount = ((SpellMatch)rule).Count;
                                    ((SpellMatchCondition)condition).DoesMatchPattern = ((SpellMatch)rule).rxDoesMatch.ToString();
                                    ((SpellMatchCondition)condition).DoesNotMatchPattern = ((SpellMatch)rule).rxDoesNotMatch.ToString();
                                    break;

                                case eLootRuleType.SpellNameMatch:
                                    condition = new SpellMatchCondition();
                                    ((SpellMatchCondition)condition).MinimumCount = 1;
                                    ((SpellMatchCondition)condition).DoesMatchPattern = ((SpellNameMatch)rule).rx.ToString();
                                    ((SpellMatchCondition)condition).DoesNotMatchPattern = "";
                                    break;

                                case eLootRuleType.StringValueMatch:
                                    condition = new StringKeyValueMatchCondition();
                                    ((StringKeyValueMatchCondition)condition).Key = (StringValueKey)(int)((StringValueMatch)rule).vk;
                                    ((StringKeyValueMatchCondition)condition).Pattern = ((StringValueMatch)rule).rx.ToString();
                                    break;

                                case eLootRuleType.TotalRatingsGE:
                                    condition = new TotalRatingsCondition();
                                    ((TotalRatingsCondition)condition).ComparisonType = ">=";
                                    ((TotalRatingsCondition)condition).Value = ((TotalRatingsGE)rule).keyval;
                                    break;

                                case eLootRuleType.UnsupportedRequirement:
                                    break;

                                default:
                                    break;
                            }

                            if (isDisabled) break;

                            if (condition != null) {
                                conditionsForRule.Add(condition);
                            }
                            else if (rule.GetRuleType() != eLootRuleType.DisabledRule) {
                                //Util.WriteToChat($"  {ir.name}::{rule.FriendlyName()} :: cant convert");
                            }
                        }
                        catch (Exception ex) {
                            Util.LogException(ex);
                            return;
                        }
                    }

                    if (!isDisabled && conditionsForRule.Count > 0) {
                        conditionCount += conditionsForRule.Count;
                        rules.Add(new VTLPRule(ir.name, conditionsForRule));
                    }
                }

                Util.WriteToChat($"Loaded {conditionCount} conditions in {rules.Count} rules");
                return;
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private string CamelAddSpace(string v) {
            return Regex.Replace(v, "(\\B[A-Z])", " $1");
        }

        public override bool NeedsIdData() {
            try {
                ParseLoadedProfile();

                foreach (var rule in rules) {
                    foreach (var condition in rule.Conditions) {
                        if (condition.NeedsIdData()) return true;
                    }
                }

                return false;
            }
            catch (Exception ex) { Util.LogException(ex); }

            return false;
        }

        public override bool MatchesItem(CachedWorldObject wo) {
            try {
                ParseLoadedProfile();

                foreach (var rule in rules) {
                    var matchesAll = true;

                    foreach (var condition in rule.Conditions) {
                        if (!condition.MatchesItem(wo)) {
                            matchesAll = false;
                            break;
                        }
                    }

                    if (matchesAll) {
                        MatchReason = rule.Name.Trim();
                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex) { Util.LogException(ex); }

            return false;
        }

        public override void DrawUI(HudFixedLayout parentLayout) {
            DrawEditConditionLayoutHeader(parentLayout, this.Name);

            profileSelectCombo = new HudCombo(new VirindiViewService.ControlGroup());
            profileSelectLabel = new HudStaticText();

            var lootProfiles = Directory.GetFiles(Util.GetLootProfilesStoragePath(), "*.utl");
            Array.Sort(lootProfiles);

            var i = 0;
            foreach (var lootProfile in lootProfiles) {
                var name = lootProfile.Replace(Util.GetLootProfilesStoragePath(), "");
                profileSelectCombo.AddItem(name, name);

                if (name == LootProfile) {
                    profileSelectCombo.Current = i;
                }

                i++;
            }

            profileSelectLabel.Text = "VTank Loot Profile: ";

            profileSelectCombo.Change += ProfileSelectCombo_Change; ;

            AddControl(parentLayout, profileSelectLabel, new Rectangle(0, 30, 110, 20));
            AddControl(parentLayout, profileSelectCombo, new Rectangle(110, 30, 300, 20));
        }

        private void ProfileSelectCombo_Change(object sender, EventArgs e) {
            try {
                LootProfile = ((HudStaticText)profileSelectCombo[profileSelectCombo.Current]).Text;
                ParseLoadedProfile();
                Globals.Plugin.mainView.filtersPage.PopulateRuleConditions();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}

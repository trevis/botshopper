﻿using BotShopper.Lib.Cache;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Decal.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using VirindiViewService.Controls;

namespace BotShopper.Lib.Conditions {
    [JsonObject(MemberSerialization.OptIn)]
    class AnySimilarColorCondition : Condition {
        [JsonProperty]
        public string HexColor = "FFFFFF";
        [JsonProperty]
        public int MaxHueDifference = 10;
        [JsonProperty]
        public double MaxSVDifference = 0.1;

        HudStaticText hexColorLabel;
        HudTextBox hexColorValue;
        HudStaticText maxHueDifferenceLabel;
        HudTextBox maxHueDifferenceValue;
        HudStaticText maxSVDifferenceLabel;
        HudTextBox maxSVDifferenceValue;

        public AnySimilarColorCondition() {
            this.Name = "One Similar Color";
        }

        public override string ToString() {
            return $"One Similar Color to #{HexColor} [MaxHueDiff {MaxHueDifference}] [MaxSVDiff {MaxSVDifference}]";
        }

        public override object Clone() {
            var clone = new AnySimilarColorCondition();

            clone.Name = Name;
            clone.HexColor = HexColor;
            clone.MaxHueDifference = MaxHueDifference;
            clone.MaxSVDifference = MaxSVDifference;

            return clone;
        }

        public override bool NeedsIdData() {
            return false; 
        }

        public override bool MatchesItem(CachedWorldObject wo) {
            try {
                if (wo.Palettes.Count == 0) return false;

                double ht, st, vt;
                var searchColor = ColorTranslator.FromHtml($"#{HexColor.Replace("#","")}");
                Util.ColorToHSV(searchColor, out ht, out st, out vt);

                foreach (var pal in wo.Palettes) {
                    double h, s, v;
                    Util.ColorToHSV(pal.Color, out h, out s, out v);
                    
                    if (Math.Abs(h - ht) > MaxHueDifference) continue;
                    
                    double ss = s - st;
                    double vv = v - vt;
                    double svdist = Math.Sqrt(ss * ss + vv * vv);
                    if (svdist > MaxSVDifference) continue;
                    
                    return true;
                }

                return false;
            }
            catch (Exception ex) { Util.LogException(ex); }

            return false;
        }

        public override void DrawUI(HudFixedLayout parentLayout) {
            DrawEditConditionLayoutHeader(parentLayout, this.Name);

            hexColorLabel = new HudStaticText();
            hexColorValue = new HudTextBox();
            maxHueDifferenceLabel = new HudStaticText();
            maxHueDifferenceValue = new HudTextBox();
            maxSVDifferenceLabel = new HudStaticText();
            maxSVDifferenceValue = new HudTextBox();

            hexColorLabel.Text = "RGB Hex Color:";
            maxHueDifferenceLabel.Text = "Max Hue Diff (0-255):";
            maxSVDifferenceLabel.Text = "Max S/V Diff (0-1):";

            hexColorValue.Text = HexColor.ToString();
            maxHueDifferenceValue.Text = MaxHueDifference.ToString(); ;
            maxSVDifferenceValue.Text = MaxSVDifference.ToString(); ;

            hexColorValue.Change += Save;
            maxHueDifferenceValue.Change += Save;
            maxSVDifferenceValue.Change += Save;

            AddControl(parentLayout, hexColorLabel, new Rectangle(0, 30, 150, 20));
            AddControl(parentLayout, hexColorValue, new Rectangle(160, 30, 250, 20));

            AddControl(parentLayout, maxHueDifferenceLabel, new Rectangle(0, 50, 150, 20));
            AddControl(parentLayout, maxHueDifferenceValue, new Rectangle(160, 50, 250, 20));

            AddControl(parentLayout, maxSVDifferenceLabel, new Rectangle(0, 70, 150, 20));
            AddControl(parentLayout, maxSVDifferenceValue, new Rectangle(160, 70, 250, 20));
        }

        private void Save(object sender, EventArgs e) {
            try {
                HexColor = hexColorValue.Text;
                if (!int.TryParse(maxHueDifferenceValue.Text, out MaxHueDifference)) {
                    maxHueDifferenceValue.Text = MaxHueDifference.ToString();
                }
                if (!double.TryParse(maxSVDifferenceValue.Text, out MaxSVDifference)) {
                    maxSVDifferenceValue.Text = MaxSVDifference.ToString();
                }

                Globals.Plugin.mainView.filtersPage.PopulateRuleConditions();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}

﻿using BotShopper.Lib.Cache;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using VirindiViewService.Controls;

namespace BotShopper.Lib.Conditions {
    [JsonObject(MemberSerialization.OptIn)]
    class FreeMainPackSpaceCondition : Condition {
        [JsonProperty]
        public string ComparisonType = ">=";
        [JsonProperty]
        public int Value = 5;

        HudCombo comparisonTypeCombo;
        HudStaticText freeMainPackSpaceLabel;
        HudTextBox freeMainPackSpaceValue;

        public FreeMainPackSpaceCondition() {
            this.Name = "Free Main Pack Space";
        }

        public override string ToString() {
            return $"Free Main Pack Space {ComparisonType} {Value}";
        }

        public override object Clone() {
            var clone = new FreeMainPackSpaceCondition();

            clone.Name = Name;
            clone.ComparisonType = ComparisonType;
            clone.Value = Value;

            return clone;
        }

        public override bool NeedsIdData() {
            return false;
        }

        public int GetFreeMainPackSpace(WorldObject container) {
            int packSlots = 102;
            
            foreach (var wo in Globals.Core.WorldFilter.GetByContainer(container.Id)) {
                if (wo != null) {
                    if (wo.ObjectClass == ObjectClass.Container) continue;
                    if (wo.ObjectClass == ObjectClass.Foci) continue;
                    if (wo.Values(LongValueKey.EquippedSlots, 0) > 0) continue;

                    --packSlots;
                }
            }

            return packSlots;
        }

        public override bool MatchesItem(CachedWorldObject wo) {
            try {
                WorldObject mainPack = Globals.Core.WorldFilter[Globals.Core.CharacterFilter.Id];

                var freeMainPackSpace = GetFreeMainPackSpace(mainPack);

                switch (ComparisonType) {
                    case "==":
                        return freeMainPackSpace == Value;
                    case "!=":
                        return freeMainPackSpace != Value;
                    case "<=":
                        return freeMainPackSpace <= Value;
                    default:
                        return freeMainPackSpace >= Value;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }

            return false;
        }

        public override void DrawUI(HudFixedLayout parentLayout) {
            DrawEditConditionLayoutHeader(parentLayout, this.Name);

            comparisonTypeCombo = new HudCombo(new VirindiViewService.ControlGroup());
            freeMainPackSpaceLabel = new HudStaticText();
            freeMainPackSpaceValue = new HudTextBox();


            var objectClasses = Enum.GetNames(typeof(ObjectClass));
            Array.Sort(objectClasses);

            var i = 0;
            foreach (var type in new List<string>() { ">=", "<=", "==", "!=" }) {
                comparisonTypeCombo.AddItem(type, type);
                if (type == ComparisonType) comparisonTypeCombo.Current = i;
                i++;
            }

            freeMainPackSpaceLabel.Text = "Free Main Pack Space";

            comparisonTypeCombo.Change += Save;
            freeMainPackSpaceValue.Change += Save;

            AddControl(parentLayout, freeMainPackSpaceLabel, new Rectangle(0, 30, 140, 20));
            AddControl(parentLayout, comparisonTypeCombo, new Rectangle(150, 30, 25, 20));
            AddControl(parentLayout, freeMainPackSpaceValue, new Rectangle(185, 30, 50, 20));
        }

        private void Save(object sender, EventArgs e) {
            try {
                if (!int.TryParse(freeMainPackSpaceValue.Text, out Value)) {
                    freeMainPackSpaceValue.Text = Value.ToString();
                }
                
                ComparisonType = ((HudStaticText)comparisonTypeCombo[comparisonTypeCombo.Current]).Text;

                Globals.Plugin.mainView.filtersPage.PopulateRuleConditions();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}

﻿using BotShopper.Lib.Cache;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Decal.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using VirindiViewService.Controls;

namespace BotShopper.Lib.Conditions {
    [JsonObject(MemberSerialization.OptIn)]
    class LongKeyValueCondition : Condition {
        [JsonProperty]
        public LongValueKey Key = LongValueKey.Unknown10;
        [JsonProperty]
        public int Value = 0;
        [JsonProperty]
        public string ComparisonType = "==";

        HudCombo longValueKeyCombo;
        HudCombo comparisonTypeCombo;
        HudTextBox valueTextBox;
        HudCombo materialsCombo;

        HudFixedLayout _parentLayout;


        internal FileService fileService = Globals.Core.Filter<FileService>();

        public static List<LongValueKey> KeysThatDontNeedIdData = new List<LongValueKey>() {
            LongValueKey.Behavior,
            LongValueKey.Category,
            LongValueKey.Container,
            LongValueKey.Coverage,
            LongValueKey.CreateFlags1,
            LongValueKey.CreateFlags2,
            LongValueKey.EquipableSlots,
            LongValueKey.EquippedSlots,
            LongValueKey.HookMask,
            LongValueKey.Icon,
            LongValueKey.IconOutline,
            LongValueKey.IconOverlay,
            LongValueKey.IconUnderlay,
            LongValueKey.ItemSlots,
            LongValueKey.Material,
            LongValueKey.MissileType,
            LongValueKey.Monarch,
            LongValueKey.PackSlots,
            LongValueKey.Slot,
            LongValueKey.StackCount,
            LongValueKey.StackMax,
            LongValueKey.Type,
            LongValueKey.Unknown10,
            LongValueKey.Unknown100000,
            LongValueKey.Unknown800000,
            LongValueKey.Unknown8000000,
            LongValueKey.UsageMask,
            LongValueKey.UsesRemaining,
            LongValueKey.UsesTotal,
            LongValueKey.Value
        };

        public LongKeyValueCondition() {
            this.Name = "Long Key Value";
        }

        private void KeyCombo_Change(object sender, EventArgs e) {
            try {
                LongValueKey key = (LongValueKey)Enum.Parse(typeof(LongValueKey), ((HudStaticText)longValueKeyCombo[longValueKeyCombo.Current]).Text);
                if ((int)key != 0) {
                    Key = key;
                    Globals.Plugin.ruleManager.Save();
                }

            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void ValueTextBox_LostFocus(object sender, EventArgs e) {
            try {
                if (int.TryParse(valueTextBox.Text, out int Value)) {
                    Globals.Plugin.ruleManager.Save();
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        public override object Clone() {
            var clone = new LongKeyValueCondition();

            clone.Name = Name;
            clone.Key = Key;
            clone.Value = Value;
            clone.ComparisonType = ComparisonType;

            return clone;
        }

        public override string ToString() {
            switch (Key) {
                case LongValueKey.Material:
                    if (Value == 0) {
                        return $"{Key} {ComparisonType} {Value}";
                    }
                    else {
                        var material = fileService.MaterialTable.GetById(Value);
                        var materialName = material == null ? Value.ToString() : material.Name;
                        return $"{Key} {ComparisonType} {materialName}";
                    }
                default:
                    return $"{Key} {ComparisonType} {Value}";
            }
        }

        public override bool NeedsIdData() {
            return !KeysThatDontNeedIdData.Contains(Key);
        }

        public override bool MatchesItem(CachedWorldObject wo) {
            var isMatch = false;

            if (wo == null) return false;

            switch (ComparisonType) {
                case ">=":
                    isMatch = wo.Values(Key, -1) >= Value;
                    break;

                case "<=":
                    isMatch = wo.Values(Key, -1) <= Value;
                    break;

                case "!=":
                    isMatch = wo.Values(Key, -1) != Value;
                    break;

                default:
                    if (Key == LongValueKey.Material && Value == 0) {
                        isMatch = wo.Values(Key, -1) == Value;
                    }
                    else {
                        isMatch = wo.Values(Key, -1) == Value;
                    }
                    break;
            }

            if (isMatch) MatchReason = ToString();

            return isMatch;
        }

        public override void DrawUI(HudFixedLayout parentLayout) {
            _parentLayout = parentLayout;
            DrawEditConditionLayoutHeader(parentLayout, this.Name);

            longValueKeyCombo = new HudCombo(new VirindiViewService.ControlGroup());
            comparisonTypeCombo = new HudCombo(new VirindiViewService.ControlGroup());

            var longValueKeys = Enum.GetNames(typeof(LongValueKey));
            Array.Sort(longValueKeys);

            var i = 0;
            foreach (var e in longValueKeys) {
                longValueKeyCombo.AddItem(e, e);
                if (Key == (LongValueKey)Enum.Parse(typeof(LongValueKey), e)) {
                    longValueKeyCombo.Current = i;
                }
                i++;
            }

            i = 0;
            foreach (var type in new List<string>() { ">=", "<=", "==", "!=" }) {
                comparisonTypeCombo.AddItem(type, type);
                if (type == ComparisonType) comparisonTypeCombo.Current = i;
                i++;
            }

            longValueKeyCombo.Change += LongValueKeyCombo_Change; ;
            comparisonTypeCombo.Change += Save;

            AddControl(parentLayout, longValueKeyCombo, new Rectangle(0, 30, 150, 20));
            AddControl(parentLayout, comparisonTypeCombo, new Rectangle(160, 30, 50, 20));

            DrawValueUI(parentLayout);
        }

        private void DrawValueUI(HudFixedLayout parentLayout) {
            switch (Key) {
                case LongValueKey.Material:
                    materialsCombo = new HudCombo(new VirindiViewService.ControlGroup());
                    
                    var materialKeys = new List<string>();

                    for (var index=0; index < fileService.MaterialTable.Length; index++) {
                        if (fileService.MaterialTable[index].Name == "Undef") continue;

                        materialKeys.Add(fileService.MaterialTable[index].Name);
                    }

                    materialKeys.Sort();

                    materialsCombo.AddItem("None", 0);

                    var i = 1;
                    foreach (var e in materialKeys) {
                        var id = fileService.MaterialTable.GetByName(e).Id;
                        materialsCombo.AddItem(e, id);
                        if (Value == id) {
                            materialsCombo.Current = i;
                        }
                        i++;
                    }

                    materialsCombo.Change += Save;
                    AddControl(parentLayout, materialsCombo, new Rectangle(220, 30, 120, 20));

                    break;

                default:
                    valueTextBox = new HudTextBox();
                    valueTextBox.Text = Value.ToString();
                    valueTextBox.Change += Save;
                    AddControl(parentLayout, valueTextBox, new Rectangle(220, 30, 100, 20));
                    break;
            }
        }

        private void LongValueKeyCombo_Change(object sender, EventArgs e) {
            try {
                Save(sender, e);
                if (_parentLayout != null) DrawUI(_parentLayout);
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void Save(object sender, EventArgs e) {
            try {
                Key = (LongValueKey)Enum.Parse(typeof(LongValueKey), ((HudStaticText)longValueKeyCombo[longValueKeyCombo.Current]).Text);
                ComparisonType = ((HudStaticText)comparisonTypeCombo[comparisonTypeCombo.Current]).Text;

                switch (Key) {
                    case LongValueKey.Material:
                        var material = fileService.MaterialTable.GetByName(((HudStaticText)materialsCombo[materialsCombo.Current]).Text);
                        Value = material == null ? 0 : material.Id;
                        break;

                    default:
                        if (!int.TryParse(valueTextBox.Text, out Value)) {
                            Util.WriteToChat($"Error converting '{valueTextBox.Text}' to int");
                            valueTextBox.Text = Value.ToString();
                        }
                        break;
                }

                Globals.Plugin.mainView.filtersPage.PopulateRuleConditions();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}

﻿using BotShopper.Lib.Cache;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Decal.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using VirindiViewService.Controls;

namespace BotShopper.Lib.Conditions {
    [JsonObject(MemberSerialization.OptIn)]
    class SpellMatchCondition : Condition {
        [JsonProperty]
        public int MinimumCount = 1;
        [JsonProperty]
        public string DoesMatchPattern = "";
        [JsonProperty]
        public string DoesNotMatchPattern = "";

        HudStaticText minimumCountLabel;
        HudTextBox minimumCountValue;
        HudStaticText matchesPatternLabel;
        HudTextBox matchesPatternValue;
        HudStaticText doesntMatchPatternLabel;
        HudTextBox doesntMatchPatternValue;

        FileService fileService = Globals.Core.Filter<FileService>();

        public SpellMatchCondition() {
            this.Name = "Spell Match";
        }

        public override string ToString() {
            var matchRules = new List<string>();

            matchRules.Add($"SpellCount >= {MinimumCount}");

            if (DoesMatchPattern.Length > 0) {
                matchRules.Add($"Matches {DoesMatchPattern}");
            }

            if (DoesNotMatchPattern.Length > 0) {
                matchRules.Add($"NotMatch {DoesNotMatchPattern}");
            }

            return String.Join(", ", matchRules.ToArray());
        }

        public override object Clone() {
            var clone = new SpellMatchCondition();

            clone.Name = Name;
            clone.MinimumCount = MinimumCount;
            clone.DoesMatchPattern = DoesMatchPattern;
            clone.DoesNotMatchPattern = DoesNotMatchPattern;

            return clone;
        }

        public override bool NeedsIdData() {
            return true;
        }

        public override bool MatchesItem(CachedWorldObject wo) {
            var matchCount = 0;

            foreach (var spellId in wo.Spells) {
                try {
                    var spell = fileService.SpellTable.GetById(spellId);

                    if (DoesMatchPattern.Length > 0 && !(new Regex(DoesMatchPattern)).IsMatch(spell.Name)) continue;
                    if (DoesNotMatchPattern.Length > 0 && (new Regex(DoesNotMatchPattern)).IsMatch(spell.Name)) continue;

                    matchCount++;

                    if (matchCount >= MinimumCount) return true;
                }
                catch (Exception ex) { Util.LogException(ex); }
            }

            return (matchCount >= MinimumCount) ? true : false;
        }

        public override void DrawUI(HudFixedLayout parentLayout) {
            DrawEditConditionLayoutHeader(parentLayout, this.Name);

            minimumCountLabel = new HudStaticText();
            minimumCountValue = new HudTextBox();
            matchesPatternLabel = new HudStaticText();
            matchesPatternValue = new HudTextBox();
            doesntMatchPatternLabel = new HudStaticText();
            doesntMatchPatternValue = new HudTextBox();

            minimumCountLabel.Text = "Minimum Spell Count:";
            matchesPatternLabel.Text = "Matches Regex:";
            doesntMatchPatternLabel.Text = "Doesn't Match Regex:";

            minimumCountValue.Text = MinimumCount.ToString();
            matchesPatternValue.Text = DoesMatchPattern;
            doesntMatchPatternValue.Text = DoesNotMatchPattern;

            minimumCountValue.Change += Save;
            matchesPatternValue.Change += Save;
            doesntMatchPatternValue.Change += Save;

            AddControl(parentLayout, minimumCountLabel, new Rectangle(0, 30, 150, 20));
            AddControl(parentLayout, minimumCountValue, new Rectangle(160, 30, 250, 20));

            AddControl(parentLayout, matchesPatternLabel, new Rectangle(0, 50, 150, 20));
            AddControl(parentLayout, matchesPatternValue, new Rectangle(160, 50, 250, 20));

            AddControl(parentLayout, doesntMatchPatternLabel, new Rectangle(0, 70, 150, 20));
            AddControl(parentLayout, doesntMatchPatternValue, new Rectangle(160, 70, 250, 20));
        }

        private void Save(object sender, EventArgs e) {
            try {
                if (!int.TryParse(minimumCountValue.Text, out MinimumCount)) {
                    minimumCountValue.Text = MinimumCount.ToString();
                }

                DoesMatchPattern = matchesPatternValue.Text;
                DoesNotMatchPattern = doesntMatchPatternValue.Text;

                Globals.Plugin.mainView.filtersPage.PopulateRuleConditions();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}

﻿using BotShopper.Lib.Cache;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Decal.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using VirindiViewService.Controls;

namespace BotShopper.Lib.Conditions {
    [JsonObject(MemberSerialization.OptIn)]
    class CalcedBuffedTinkedTargetWeaponModCondition : Condition {
        [JsonProperty]
        public double TargetCalcedBuffedDoT = 0;
        [JsonProperty]
        public double TargetBuffedMelee = 0;
        [JsonProperty]
        public double TargetBuffedAttackBonus = 0;

        HudStaticText targetCalcedBuffedLabel;
        HudTextBox targetCalcedBuffedValue;
        HudStaticText targetBuffedMeleeLabel;
        HudTextBox targetBuffedMeleeValue;
        HudStaticText targetBuffedAttackBonusLabel;
        HudTextBox targetBuffedAttackBonusValue;

        FileService fileService = Globals.Core.Filter<FileService>();

        public CalcedBuffedTinkedTargetWeaponModCondition() {
            this.Name = "CalcedBuffedTinkedTargetMelee";
        }

        public override string ToString() {
            var matchRules = new List<string>();

            matchRules.Add($"CalcedBuffedDoT >= {TargetCalcedBuffedDoT}");
            matchRules.Add($"BuffedMelee >= {TargetBuffedMelee}");
            matchRules.Add($"BuffedAttackBonus >= {TargetBuffedAttackBonus}");

            return String.Join(", ", matchRules.ToArray());
        }

        public override object Clone() {
            var clone = new CalcedBuffedTinkedTargetWeaponModCondition();

            clone.Name = Name;
            clone.TargetCalcedBuffedDoT = TargetCalcedBuffedDoT;
            clone.TargetBuffedMelee = TargetBuffedMelee;
            clone.TargetBuffedAttackBonus = TargetBuffedAttackBonus;

            return clone;
        }

        public override bool NeedsIdData() {
            return true;
        }

        public override bool MatchesItem(CachedWorldObject wo) {
            try {
                return wo.CanReachTargetValues(TargetCalcedBuffedDoT, TargetBuffedMelee, TargetBuffedAttackBonus);
            }
            catch (Exception ex) { Util.LogException(ex); }

            return false;
        }

        public override void DrawUI(HudFixedLayout parentLayout) {
            DrawEditConditionLayoutHeader(parentLayout, this.Name);

            targetCalcedBuffedLabel = new HudStaticText();
            targetCalcedBuffedValue = new HudTextBox();
            targetBuffedMeleeLabel = new HudStaticText();
            targetBuffedMeleeValue = new HudTextBox();
            targetBuffedAttackBonusLabel = new HudStaticText();
            targetBuffedAttackBonusValue = new HudTextBox();

            targetCalcedBuffedLabel.Text = "Target Calced Buffed DoT:";
            targetBuffedMeleeLabel.Text = "Target Buffed MeleeD:";
            targetBuffedAttackBonusLabel.Text = "Buffed Attack Bonus:";

            targetCalcedBuffedValue.Text = TargetCalcedBuffedDoT.ToString();
            targetBuffedMeleeValue.Text = TargetBuffedMelee.ToString(); ;
            targetBuffedAttackBonusValue.Text = TargetBuffedAttackBonus.ToString(); ;

            targetCalcedBuffedValue.Change += Save;
            targetBuffedMeleeValue.Change += Save;
            targetBuffedAttackBonusValue.Change += Save;

            AddControl(parentLayout, targetCalcedBuffedLabel, new Rectangle(0, 30, 150, 20));
            AddControl(parentLayout, targetCalcedBuffedValue, new Rectangle(160, 30, 250, 20));

            AddControl(parentLayout, targetBuffedMeleeLabel, new Rectangle(0, 50, 150, 20));
            AddControl(parentLayout, targetBuffedMeleeValue, new Rectangle(160, 50, 250, 20));

            AddControl(parentLayout, targetBuffedAttackBonusLabel, new Rectangle(0, 70, 150, 20));
            AddControl(parentLayout, targetBuffedAttackBonusValue, new Rectangle(160, 70, 250, 20));
        }

        private void Save(object sender, EventArgs e) {
            try {
                if (!double.TryParse(targetCalcedBuffedValue.Text, out TargetCalcedBuffedDoT)) {
                    targetCalcedBuffedValue.Text = TargetCalcedBuffedDoT.ToString();
                }
                if (!double.TryParse(targetBuffedMeleeValue.Text, out TargetBuffedMelee)) {
                    targetBuffedMeleeValue.Text = TargetBuffedMelee.ToString();
                }
                if (!double.TryParse(targetBuffedAttackBonusValue.Text, out TargetBuffedAttackBonus)) {
                    targetBuffedAttackBonusValue.Text = TargetBuffedAttackBonus.ToString();
                }

                Globals.Plugin.mainView.filtersPage.PopulateRuleConditions();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}

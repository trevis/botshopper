﻿using BotShopper.Lib.Cache;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using VirindiViewService.Controls;

namespace BotShopper.Lib.Conditions {
    [JsonObject(MemberSerialization.OptIn)]
    class BuffedMissileDamageCondition : Condition {
        [JsonProperty]
        public string ComparisonType = ">=";
        [JsonProperty]
        public double Value = 20;

        HudCombo comparisonTypeCombo;
        HudStaticText buffedMissileDamageLabel;
        HudTextBox buffedMissileDamageValue;

        public BuffedMissileDamageCondition() {
            this.Name = "Buffed Missile Damage";
        }

        public override string ToString() {
            return $"Buffed Missile Damage {ComparisonType} {Value}";
        }

        public override object Clone() {
            var clone = new TotalRatingsCondition();

            clone.Name = Name;
            clone.ComparisonType = ComparisonType;
            clone.Value = Value;

            return clone;
        }

        public override bool NeedsIdData() {
            return false;
        }

        public override bool MatchesItem(CachedWorldObject wo) {
            try {
                var damage = wo.BuffedMissileDamage;

                switch (ComparisonType) {
                    case "==":
                        return damage == Value;
                    case "!=":
                        return damage != Value;
                    case "<=":
                        return damage <= Value;
                    case ">=":
                        return damage >= Value;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }

            return false;
        }

        public override void DrawUI(HudFixedLayout parentLayout) {
            DrawEditConditionLayoutHeader(parentLayout, this.Name);

            comparisonTypeCombo = new HudCombo(new VirindiViewService.ControlGroup());
            buffedMissileDamageLabel = new HudStaticText();
            buffedMissileDamageValue = new HudTextBox();


            var objectClasses = Enum.GetNames(typeof(ObjectClass));
            Array.Sort(objectClasses);

            var i = 0;
            foreach (var type in new List<string>() { ">=", "<=", "==", "!=" }) {
                comparisonTypeCombo.AddItem(type, type);
                if (type == ComparisonType) comparisonTypeCombo.Current = i;
                i++;
            }

            buffedMissileDamageLabel.Text = "Buffed Missile Damage";

            comparisonTypeCombo.Change += Save;
            buffedMissileDamageValue.Change += Save;

            AddControl(parentLayout, buffedMissileDamageLabel, new Rectangle(0, 30, 140, 20));
            AddControl(parentLayout, comparisonTypeCombo, new Rectangle(150, 30, 25, 20));
            AddControl(parentLayout, buffedMissileDamageValue, new Rectangle(185, 30, 50, 20));
        }

        private void Save(object sender, EventArgs e) {
            try {
                if (!double.TryParse(buffedMissileDamageValue.Text, out Value)) {
                    buffedMissileDamageValue.Text = Value.ToString();
                }

                ComparisonType = ((HudStaticText)comparisonTypeCombo[comparisonTypeCombo.Current]).Text;

                Globals.Plugin.mainView.filtersPage.PopulateRuleConditions();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}

﻿using BotShopper.Lib.Cache;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Decal.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using VirindiViewService.Controls;

namespace BotShopper.Lib.Conditions {
    [JsonObject(MemberSerialization.OptIn)]
    class StringKeyValueMatchCondition : Condition {
        [JsonProperty]
        public StringValueKey Key = StringValueKey.Name;
        [JsonProperty]
        public string Pattern = "";

        HudCombo stringValueKeyCombo;
        HudTextBox pattern;

        HudFixedLayout _parentLayout;


        FileService fileService = Globals.Core.Filter<FileService>();

        public static List<StringValueKey> KeysThatDontNeedIdData = new List<StringValueKey>() {
            StringValueKey.Name,
            StringValueKey.SecondaryName
        };

        public StringKeyValueMatchCondition() {
            this.Name = "String Key Value Match";
        }

        public override object Clone() {
            var clone = new StringKeyValueMatchCondition();

            clone.Name = Name;
            clone.Key = Key;
            clone.Pattern = Pattern;

            return clone;
        }

        public override string ToString() {
            return $"{Key} matches {Pattern}";
        }

        public override bool NeedsIdData() {
            return !KeysThatDontNeedIdData.Contains(Key);
        }

        public override bool MatchesItem(CachedWorldObject wo) {
            try {
                if (wo == null) return false;

                var matchPattern = new Regex(Pattern, RegexOptions.IgnoreCase);
                var matchAgainst = wo.Values(Key, "");

                if (Key == StringValueKey.Name) {
                    matchAgainst = Util.GetFullItemName(wo);
                }

                return matchPattern.IsMatch(matchAgainst);
            }
            catch (Exception ex) { Util.LogException(ex); }

            return false;
        }

        public override void DrawUI(HudFixedLayout parentLayout) {
            _parentLayout = parentLayout;
            DrawEditConditionLayoutHeader(parentLayout, this.Name);

            var patternLabel = new HudStaticText();
            stringValueKeyCombo = new HudCombo(new VirindiViewService.ControlGroup());
            pattern = new HudTextBox();

            var stringValueKeys = Enum.GetNames(typeof(StringValueKey));
            Array.Sort(stringValueKeys);

            var i = 0;
            foreach (var e in stringValueKeys) {
                stringValueKeyCombo.AddItem(e, e);
                if (!KeysThatDontNeedIdData.Contains((StringValueKey)Enum.Parse(typeof(StringValueKey), e))) {
                    ((HudStaticText)stringValueKeyCombo[i]).TextColor = Color.Red;
                }
                if (Key == (StringValueKey)Enum.Parse(typeof(StringValueKey), e)) {
                    stringValueKeyCombo.Current = i;
                }
                i++;
            }
            pattern.Text = Pattern;
            patternLabel.Text = "matches regex pattern:";

            stringValueKeyCombo.Change += Save;
            pattern.Change += Save;

            AddControl(parentLayout, stringValueKeyCombo, new Rectangle(0, 30, 150, 20));
            AddControl(parentLayout, patternLabel, new Rectangle(160, 30, 120, 20));
            AddControl(parentLayout, pattern, new Rectangle(0, 50, 400, 20));
        }

        private void Save(object sender, EventArgs e) {
            try {
                Key = (StringValueKey)Enum.Parse(typeof(StringValueKey), ((HudStaticText)stringValueKeyCombo[stringValueKeyCombo.Current]).Text);
                // TODO: test valid regex before saving?
                Pattern = pattern.Text;

                Globals.Plugin.mainView.filtersPage.PopulateRuleConditions();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}

﻿using BotShopper.Lib.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BotShopper.Lib {
    static class ItemCache {
        static Dictionary<int, CachedWorldObject> cache = new Dictionary<int, CachedWorldObject>();

        public static CachedWorldObject Get(int id) {
            if (cache.ContainsKey(id)) {
                cache[id].SaveToCache();
                return cache[id];
            }

            cache.Add(id, new CachedWorldObject(id));

            return Get(id);
        }

        public static bool Contains(int id) {
            return cache.ContainsKey(id);
        }

        public static void Clear() {
            cache.Clear();
        }
    }
}

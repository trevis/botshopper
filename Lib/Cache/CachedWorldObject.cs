﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Decal.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace BotShopper.Lib.Cache {
    struct SpellInfo<T> {
        public readonly T Key;
        public readonly double Change;
        public readonly double Bonus;

        public SpellInfo(T key, double change)
            : this(key, change, 0) {
        }

        public SpellInfo(T key, double change, double bonus) {
            Key = key;
            Change = change;
            Bonus = bonus;
        }
    }

    public class PaletteData {
        public int Palette { get; }
        public byte Offset { get; }
        public byte Length { get; }
        public Color ExampleColor { get; }
        public string RGB { get; }
        public Color Color { get; set; }

        public PaletteData(int palette, int offset, int length) {
            Palette = palette;
            Offset = (byte)offset;
            Length = (byte)length;
            
            var p = PaletteColorInfo.Get(Palette, offset, length);

            if (p != null) {
                ExampleColor = p.ExampleColor;
                RGB = p.RGB;
                Color = p.Color;
            }
            else {
                Util.WriteToChat("Could not get color from: Palette:" + Palette);
            }
        }
    }

    class CachedWorldObject {
        public int Id { get; set; }

        public string Name { get; set; }
        public int Icon { get; set; }
        public int Behavior { get; set; }
        public int Category { get; set; }
        public int Container { get; set; }
        public int GameDataFlags1 { get; set; }
        public ObjectClass ObjectClass { get; set; }
        public int PhysicsDataFlags { get; set; }
        public int Type { get; set; }

        public bool HasIdData { get; set; }
        public bool IsFromCache { get; set; }
        public DateTime LastCached { get; set; }
        public DateTime LastId { get; set; }

        public Dictionary<int, int> LongKeyValues = new Dictionary<int, int>();
        public Dictionary<int, string> StringKeyValues = new Dictionary<int, string>();
        public Dictionary<int, bool> BoolKeyValues = new Dictionary<int, bool>();
        public Dictionary<int, double> DoubleKeyValues = new Dictionary<int, double>();

        public List<int> ActiveSpells = new List<int>();
        public List<int> Spells = new List<int>();

        public List<PaletteData> Palettes = new List<PaletteData>();

        private string lastData = "";
        private bool hasNewData = false;

        static bool didInit = false;
        static readonly Dictionary<int, SpellInfo<LongValueKey>> LongValueKeySpellEffects = new Dictionary<int, SpellInfo<LongValueKey>>();
        static readonly Dictionary<int, SpellInfo<DoubleValueKey>> DoubleValueKeySpellEffects = new Dictionary<int, SpellInfo<DoubleValueKey>>();

        public CachedWorldObject() {
            HasIdData = false;
            IsFromCache = false;
            Init();
        }

        public CachedWorldObject(int id) {
            Init();
            Id = id;

            IsFromCache = false;
            HasIdData = false;

            LoadFromCache();

            try {
                var wo = CoreManager.Current.WorldFilter[Id];

                if (wo != null) {
                    LoadFromWorldObject(wo);
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private static void Init() {
            if (didInit) return;
            didInit = true;
            LongValueKeySpellEffects[1616] = new SpellInfo<LongValueKey>(LongValueKey.MaxDamage, 20); // Blood Drinker VI
            LongValueKeySpellEffects[2096] = new SpellInfo<LongValueKey>(LongValueKey.MaxDamage, 22); // Infected Caress
            LongValueKeySpellEffects[5183] = new SpellInfo<LongValueKey>(LongValueKey.MaxDamage, 24); // Incantation of Blood Drinker Post Feb-2013
            LongValueKeySpellEffects[4395] = new SpellInfo<LongValueKey>(LongValueKey.MaxDamage, 24); // Incantation of Blood Drinker Post Feb-2013
            LongValueKeySpellEffects[2598] = new SpellInfo<LongValueKey>(LongValueKey.MaxDamage, 2, 2); // Minor Blood Thirst
            LongValueKeySpellEffects[2586] = new SpellInfo<LongValueKey>(LongValueKey.MaxDamage, 4, 4); // Major Blood Thirst
            LongValueKeySpellEffects[4661] = new SpellInfo<LongValueKey>(LongValueKey.MaxDamage, 7, 7); // Epic Blood Thirst
            LongValueKeySpellEffects[6089] = new SpellInfo<LongValueKey>(LongValueKey.MaxDamage, 10, 10); // Legendary Blood Thirst
            LongValueKeySpellEffects[3688] = new SpellInfo<LongValueKey>(LongValueKey.MaxDamage, 300); // Prodigal Blood Drinker
            LongValueKeySpellEffects[2604] = new SpellInfo<LongValueKey>(LongValueKey.ArmorLevel, 20, 20); // Minor Impenetrability
            LongValueKeySpellEffects[2592] = new SpellInfo<LongValueKey>(LongValueKey.ArmorLevel, 40, 40); // Major Impenetrability
            LongValueKeySpellEffects[4667] = new SpellInfo<LongValueKey>(LongValueKey.ArmorLevel, 60, 60); // Epic Impenetrability
            LongValueKeySpellEffects[6095] = new SpellInfo<LongValueKey>(LongValueKey.ArmorLevel, 80, 80); // Legendary Impenetrability



            DoubleValueKeySpellEffects[3258] = new SpellInfo<DoubleValueKey>(DoubleValueKey.ElementalDamageVersusMonsters, .06); // Spirit Drinker VI
            DoubleValueKeySpellEffects[3259] = new SpellInfo<DoubleValueKey>(DoubleValueKey.ElementalDamageVersusMonsters, .07); // Infected Spirit Caress
            DoubleValueKeySpellEffects[5182] = new SpellInfo<DoubleValueKey>(DoubleValueKey.ElementalDamageVersusMonsters, .08); // Incantation of Spirit Drinker Post Feb-2013
            DoubleValueKeySpellEffects[4414] = new SpellInfo<DoubleValueKey>(DoubleValueKey.ElementalDamageVersusMonsters, .08); // Incantation of Spirit Drinker, this spell on the item adds 1 more % of damage over a user casted 8 Post Feb-2013

            DoubleValueKeySpellEffects[3251] = new SpellInfo<DoubleValueKey>(DoubleValueKey.ElementalDamageVersusMonsters, .01, .01); // Minor Spirit Thirst
            DoubleValueKeySpellEffects[3250] = new SpellInfo<DoubleValueKey>(DoubleValueKey.ElementalDamageVersusMonsters, .03, .03); // Major Spirit Thirst
            DoubleValueKeySpellEffects[4670] = new SpellInfo<DoubleValueKey>(DoubleValueKey.ElementalDamageVersusMonsters, .05, .05); // Epic Spirit Thirst
            DoubleValueKeySpellEffects[6098] = new SpellInfo<DoubleValueKey>(DoubleValueKey.ElementalDamageVersusMonsters, .07, .07); // Legendary Spirit Thirst

            DoubleValueKeySpellEffects[3735] = new SpellInfo<DoubleValueKey>(DoubleValueKey.ElementalDamageVersusMonsters, .15); // Prodigal Spirit Drinker


            DoubleValueKeySpellEffects[1592] = new SpellInfo<DoubleValueKey>(DoubleValueKey.AttackBonus, .15); // Heart Seeker VI
            DoubleValueKeySpellEffects[2106] = new SpellInfo<DoubleValueKey>(DoubleValueKey.AttackBonus, .17); // Elysa's Sight
            DoubleValueKeySpellEffects[4405] = new SpellInfo<DoubleValueKey>(DoubleValueKey.AttackBonus, .20); // Incantation of Heart Seeker

            DoubleValueKeySpellEffects[2603] = new SpellInfo<DoubleValueKey>(DoubleValueKey.AttackBonus, .03, .03); // Minor Heart Thirst
            DoubleValueKeySpellEffects[2591] = new SpellInfo<DoubleValueKey>(DoubleValueKey.AttackBonus, .05, .05); // Major Heart Thirst
            DoubleValueKeySpellEffects[4666] = new SpellInfo<DoubleValueKey>(DoubleValueKey.AttackBonus, .07, .07); // Epic Heart Thirst
            DoubleValueKeySpellEffects[6094] = new SpellInfo<DoubleValueKey>(DoubleValueKey.AttackBonus, .09, .09); // Legendary Heart Thirst


            DoubleValueKeySpellEffects[1605] = new SpellInfo<DoubleValueKey>(DoubleValueKey.MeleeDefenseBonus, .15); // Defender VI
            DoubleValueKeySpellEffects[2101] = new SpellInfo<DoubleValueKey>(DoubleValueKey.MeleeDefenseBonus, .17); // Cragstone's Will
            DoubleValueKeySpellEffects[4400] = new SpellInfo<DoubleValueKey>(DoubleValueKey.MeleeDefenseBonus, .20); // Incantation of Defender Post Feb-2013

            DoubleValueKeySpellEffects[2600] = new SpellInfo<DoubleValueKey>(DoubleValueKey.MeleeDefenseBonus, .03, .03); // Minor Defender
            DoubleValueKeySpellEffects[3985] = new SpellInfo<DoubleValueKey>(DoubleValueKey.MeleeDefenseBonus, .04, .04); // Mukkir Sense
            DoubleValueKeySpellEffects[2588] = new SpellInfo<DoubleValueKey>(DoubleValueKey.MeleeDefenseBonus, .05, .05); // Major Defender
            DoubleValueKeySpellEffects[4663] = new SpellInfo<DoubleValueKey>(DoubleValueKey.MeleeDefenseBonus, .07, .07); // Epic Defender
            DoubleValueKeySpellEffects[6091] = new SpellInfo<DoubleValueKey>(DoubleValueKey.MeleeDefenseBonus, .09, .09); // Legendary Defender

            DoubleValueKeySpellEffects[3699] = new SpellInfo<DoubleValueKey>(DoubleValueKey.MeleeDefenseBonus, .25); // Prodigal Defender


            DoubleValueKeySpellEffects[1480] = new SpellInfo<DoubleValueKey>(DoubleValueKey.ManaCBonus, 1.60); // Hermetic Link VI
            DoubleValueKeySpellEffects[2117] = new SpellInfo<DoubleValueKey>(DoubleValueKey.ManaCBonus, 1.70); // Mystic's Blessing
            DoubleValueKeySpellEffects[4418] = new SpellInfo<DoubleValueKey>(DoubleValueKey.ManaCBonus, 1.80); // Incantation of Hermetic Link

            DoubleValueKeySpellEffects[3201] = new SpellInfo<DoubleValueKey>(DoubleValueKey.ManaCBonus, 1.05, 1.05); // Feeble Hermetic Link
            DoubleValueKeySpellEffects[3199] = new SpellInfo<DoubleValueKey>(DoubleValueKey.ManaCBonus, 1.10, 1.10); // Minor Hermetic Link
            DoubleValueKeySpellEffects[3202] = new SpellInfo<DoubleValueKey>(DoubleValueKey.ManaCBonus, 1.15, 1.15); // Moderate Hermetic Link
            DoubleValueKeySpellEffects[3200] = new SpellInfo<DoubleValueKey>(DoubleValueKey.ManaCBonus, 1.20, 1.20); // Major Hermetic Link
            DoubleValueKeySpellEffects[6086] = new SpellInfo<DoubleValueKey>(DoubleValueKey.ManaCBonus, 1.25, 1.25); // Epic Hermetic Link
            DoubleValueKeySpellEffects[6087] = new SpellInfo<DoubleValueKey>(DoubleValueKey.ManaCBonus, 1.30, 1.30); // Legendary Hermetic Link
        }

        internal void SetPalettes(List<PaletteData> paletteList) {
            if (Palettes.Count != paletteList.Count) {
                hasNewData = true;
                Palettes = paletteList;
                SaveToCache();
            }
        }

        public int Values(LongValueKey key, int defaultValue = -1) {
            return LongKeyValues.ContainsKey((int)key) ? LongKeyValues[(int)key] : defaultValue;
        }

        public string Values(StringValueKey key, string defaultValue = "") {
            return StringKeyValues.ContainsKey((int)key) ? StringKeyValues[(int)key] : defaultValue;
        }

        public bool Values(BoolValueKey key, bool defaultValue = false) {
            return BoolKeyValues.ContainsKey((int)key) ? BoolKeyValues[(int)key] : defaultValue;
        }

        public double Values(DoubleValueKey key, double defaultValue = -1) {
            return DoubleKeyValues.ContainsKey((int)key) ? DoubleKeyValues[(int)key] : defaultValue;
        }

        public bool ValueExists(LongValueKey key) {
            return LongKeyValues.ContainsKey((int)key);
        }

        public bool ValueExists(StringValueKey key) {
            return StringKeyValues.ContainsKey((int)key);
        }

        public bool ValueExists(BoolValueKey key) {
            return BoolKeyValues.ContainsKey((int)key);
        }

        public bool ValueExists(DoubleValueKey key) {
            return DoubleKeyValues.ContainsKey((int)key);
        }

        // from VTClassic/ComputedItemInfo.cs
        public double BuffedAverageDamage {
            get {
                double variance = Values(DoubleValueKey.Variance, 0.0);
                int maxDamage = GetBuffedLongValueKey(LongValueKey.MaxDamage);
                double minDamage = maxDamage - (variance * maxDamage);

                return (minDamage + maxDamage) / 2;
            }
        }

        // from VTClassic/ComputedItemInfo.cs
        public double CalcedBuffedTinkedDamage {
            get {
                double variance = Values(DoubleValueKey.Variance, 0.0);
                int maxDamage = GetBuffedLongValueKey(LongValueKey.MaxDamage);

                int numberOfTinksLeft = Math.Max(10 - Values(LongValueKey.NumberTimesTinkered, 0), 0);

                if (Values(LongValueKey.Imbued, 0) == 0)
                    numberOfTinksLeft--; // Factor in an imbue tink

                // If this is not a loot generated item, it can't be tinked
                if (Values(LongValueKey.Material, 0) == 0)
                    numberOfTinksLeft = 0;

                for (int i = 1; i <= numberOfTinksLeft; i++) {
                    double ironTinkDoT = CalculateDamageOverTime(maxDamage + 24 + 1, variance);
                    double graniteTinkDoT = CalculateDamageOverTime(maxDamage + 24, variance * .8);

                    if (ironTinkDoT >= graniteTinkDoT)
                        maxDamage++;
                    else
                        variance *= .8;
                }

                return CalculateDamageOverTime(maxDamage + 24, variance);
            }
        }

        // from VTClassic/ComputedItemInfo.cs
        public bool CanReachTargetValues(double targetCalcedBuffedTinkedDoT, double targetBuffedMeleeDefenseBonus, double targetBuffedAttackBonus) {
            double buffedMeleeDefenseBonus = GetBuffedDoubleValueKey(DoubleValueKey.MeleeDefenseBonus);
            double buffedAttackBonus = GetBuffedDoubleValueKey(DoubleValueKey.AttackBonus);

            double variance = Values(DoubleValueKey.Variance, 0.0);
            int maxDamage = GetBuffedLongValueKey(LongValueKey.MaxDamage);

            int numberOfTinksLeft = Math.Max(10 - Values(LongValueKey.NumberTimesTinkered, 0), 0);

            if (Values(LongValueKey.Imbued, 0) == 0)
                numberOfTinksLeft--; // Factor in an imbue tink

            // If this is not a loot generated item, it can't be tinked
            if (Values(LongValueKey.Material, 0) == 0)
                numberOfTinksLeft = 0;

            for (int i = 1; i <= numberOfTinksLeft; i++) {
                if (buffedMeleeDefenseBonus < targetBuffedMeleeDefenseBonus)
                    buffedMeleeDefenseBonus += .01;
                else if (buffedAttackBonus < targetBuffedAttackBonus)
                    buffedAttackBonus += .01;
                else {
                    double ironTinkDoT = CalculateDamageOverTime(maxDamage + 24 + 1, variance);
                    double graniteTinkDoT = CalculateDamageOverTime(maxDamage + 24, variance * .8);

                    if (ironTinkDoT >= graniteTinkDoT)
                        maxDamage++;
                    else
                        variance *= .8;
                }
            }

            return CalculateDamageOverTime(maxDamage + 24, variance) >= targetCalcedBuffedTinkedDoT && buffedMeleeDefenseBonus >= targetBuffedMeleeDefenseBonus && buffedAttackBonus >= targetBuffedAttackBonus;
        }

        // from VTClassic/ComputedItemInfo.cs
        public int TotalRatings {
            get {
                /*
                DamRating = 370,
                DamResRating = 371,
                CritRating = 372,
                CritResistRating = 373,
                CritDamRating = 374,
                CritDamResistRating = 375,
                HealBoostRating = 376,
                VitalityRating = 379,
                */
                return Values((LongValueKey)370, 0) + Values((LongValueKey)371, 0) + Values((LongValueKey)372, 0) + Values((LongValueKey)373, 0) + Values((LongValueKey)374, 0) + Values((LongValueKey)375, 0) + Values((LongValueKey)376, 0) + Values((LongValueKey)379, 0);
            }
        }

        // from VTClassic/ComputedItemInfo.cs
        public static double CalculateDamageOverTime(int maxDamage, double variance) {
            return CalculateDamageOverTime(maxDamage, variance, 0.1d, 2d);
        }

        // from VTClassic/ComputedItemInfo.cs
        /// <summary>
        /// maxDamage * ((1 - critChance) * (2 - variance) / 2 + (critChance * critMultiplier));
        /// </summary>
        /// <param name="maxDamage"></param>
        /// <param name="variance"></param>
        /// <param name="critChance"></param>
        /// <param name="critMultiplier"></param>
        /// <returns></returns>
        public static double CalculateDamageOverTime(int maxDamage, double variance, double critChance, double critMultiplier) {
            return maxDamage * ((1 - critChance) * (2 - variance) / 2 + (critChance * critMultiplier));
        }

        // from VTClassic/ComputedItemInfo.cs
        public double BuffedMissileDamage {
            get {
                return GetBuffedLongValueKey(LongValueKey.MaxDamage) + (((GetBuffedDoubleValueKey(DoubleValueKey.DamageBonus) - 1) * 100) / 3) + GetBuffedLongValueKey(LongValueKey.ElementalDmgBonus);
            }
        }

        // from VTClassic/ComputedItemInfo.cs
        public int GetBuffedLongValueKey(LongValueKey key) {
            return GetBuffedLongValueKey(key, 0);
        }

        // from VTClassic/ComputedItemInfo.cs
        public int GetBuffedLongValueKey(LongValueKey key, int defaultValue) {
            if (!ValueExists(key))
                return defaultValue;

            int value = Values(key, defaultValue);

            for (int i = 0; i < Spells.Count; i++) {
                int spellId = Spells[i];

                if (LongValueKeySpellEffects.ContainsKey(spellId) && LongValueKeySpellEffects[spellId].Key == key && LongValueKeySpellEffects[spellId].Bonus != 0)
                    value += (int)LongValueKeySpellEffects[spellId].Bonus;
            }

            return value;
        }

        // from VTClassic/ComputedItemInfo.cs
        public double GetBuffedDoubleValueKey(DoubleValueKey key) {
            return GetBuffedDoubleValueKey(key, 0d);
        }

        // from VTClassic/ComputedItemInfo.cs
        public double GetBuffedDoubleValueKey(DoubleValueKey key, double defaultValue) {
            if (!ValueExists(key))
                return defaultValue;

            double value = Values(key, defaultValue);

            for (int i = 0; i < Spells.Count; i++) {
                int spellId = Spells[i];

                if (DoubleValueKeySpellEffects.ContainsKey(spellId) && DoubleValueKeySpellEffects[spellId].Key == key && DoubleValueKeySpellEffects[spellId].Bonus != 0) {
                    if ((int)DoubleValueKeySpellEffects[spellId].Change == 1)
                        value *= DoubleValueKeySpellEffects[spellId].Bonus;
                    else
                        value += DoubleValueKeySpellEffects[spellId].Bonus;
                }
            }

            return value;
        }

        internal string GetGameDisplayName() {
            if (Values(LongValueKey.Material, 0) > 0) {
                FileService service = Globals.Core.Filter<FileService>();
                var material = service.MaterialTable.GetById(Values(LongValueKey.Material, 0));
                return $"{material} {Name}";
            }
            else {
                return Name;
            }
        }

        internal string GetTradeName() {
            var value = Values(LongValueKey.Value);

            if (Values(LongValueKey.Material, 0) > 0) {
                FileService service = Globals.Core.Filter<FileService>();
                var material = service.MaterialTable.GetById(Values(LongValueKey.Material, 0));
                return $"{material} {Name}:{value}";
            }
            else {
                return $"{Name}:{value}";
            }
        }

        public void LoadFromCache() {
            try {
                if (!File.Exists(Util.GetItemCachePath(Id))) return;

                var json = File.ReadAllText(Util.GetItemCachePath(Id));
                
                var cacheItem = JsonConvert.DeserializeObject<CachedWorldObject>(json);

                if (cacheItem != null) {
                    IsFromCache = true;

                    HasIdData = cacheItem.HasIdData;

                    Id = cacheItem.Id;
                    Name = cacheItem.Name;
                    Icon = cacheItem.Icon;
                    Behavior = cacheItem.Behavior;
                    Category = cacheItem.Category;
                    Container = cacheItem.Container;
                    GameDataFlags1 = cacheItem.GameDataFlags1;
                    PhysicsDataFlags = cacheItem.PhysicsDataFlags;
                    Type = cacheItem.Type;
                    ObjectClass = cacheItem.ObjectClass;

                    if (cacheItem.HasIdData) {
                        BoolKeyValues = cacheItem.BoolKeyValues;
                        DoubleKeyValues = cacheItem.DoubleKeyValues;
                        LongKeyValues = cacheItem.LongKeyValues;
                        StringKeyValues = cacheItem.StringKeyValues;
                    }

                    if (cacheItem.Spells.Count > 0) Spells = cacheItem.Spells;
                    if (cacheItem.Spells.Count > 0) ActiveSpells = cacheItem.ActiveSpells;
                    if (cacheItem.Spells.Count > 0 && Palettes.Count == 0) Palettes = cacheItem.Palettes;
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        public void SaveToCache() {
            try {
                if (!HasIdData) return;
                if (!hasNewData) return;

                LastCached = DateTime.UtcNow;

                var newData = JsonConvert.SerializeObject(this, Formatting.Indented);

                if (lastData == newData) return;
                lastData = newData;

                File.WriteAllText(Util.GetItemCachePath(Id), newData);
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        public void LoadFromWorldObject(WorldObject wo) {
            try {
                HasIdData = wo.HasIdData || HasIdData;

                Name = wo.Name;
                ObjectClass = wo.ObjectClass;
                Icon = wo.Icon;
                Behavior = wo.Behavior;
                Category = wo.Category;
                Container = wo.Container;
                GameDataFlags1 = wo.GameDataFlags1;
                PhysicsDataFlags = wo.PhysicsDataFlags;
                Type = wo.Type;


                hasNewData = wo.HasIdData;

                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddSeconds(wo.LastIdTime).ToLocalTime();
                LastId = dtDateTime;

                for (var i = 0; i < wo.SpellCount; i++) {
                    if (!Spells.Contains(wo.Spell(i))) Spells.Add(wo.Spell(i));
                }

                for (var i = 0; i < wo.ActiveSpellCount; i++) {
                    if (!ActiveSpells.Contains(wo.Spell(i))) ActiveSpells.Add(wo.ActiveSpell(i));
                }

                foreach (var key in wo.BoolKeys) {
                    if (BoolKeyValues.ContainsKey(key)) {
                        BoolKeyValues[key] = wo.Values((BoolValueKey)key);
                    }
                    else {
                        BoolKeyValues.Add(key, wo.Values((BoolValueKey)key));
                    }
                }

                foreach (var key in wo.DoubleKeys) {
                    if (DoubleKeyValues.ContainsKey(key)) {
                        DoubleKeyValues[key] = wo.Values((DoubleValueKey)key);
                    }
                    else {
                        DoubleKeyValues.Add(key, wo.Values((DoubleValueKey)key));
                    }
                }

                foreach (var key in wo.LongKeys) {
                    if (LongKeyValues.ContainsKey(key)) {
                        LongKeyValues[key] = wo.Values((LongValueKey)key);
                    }
                    else {
                        LongKeyValues.Add(key, wo.Values((LongValueKey)key));
                    }
                }

                foreach (var key in wo.StringKeys) {
                    if (StringKeyValues.ContainsKey(key)) {
                        StringKeyValues[key] = wo.Values((StringValueKey)key);
                    }
                    else {
                        StringKeyValues.Add(key, wo.Values((StringValueKey)key));
                    }
                }

                for (int i = 0; i < wo.ActiveSpellCount; i++) {
                    var spell = wo.ActiveSpell(i);

                    if (!ActiveSpells.Contains(spell)) {
                        ActiveSpells.Add(spell);
                    }
                }

                for (int i = 0; i < wo.SpellCount; i++) {
                    var spell = wo.Spell(i);

                    if (!Spells.Contains(spell)) {
                        Spells.Add(spell);
                    }
                }

                SaveToCache();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}

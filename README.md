## BotShopper
A tool to facilitate shopping at trade bots.  It has a built in loot rule system with an in-game editor.
![](https://i.gyazo.com/67bb193b7b058da5faf4beaf96ce64ed.mp4)

### Instructions
* Install [BotShopperInstaller-1.0.1.1.exe](/uploads/8e5685f83c527beb0f2399da9c8d966c/BotShopperInstaller-1.0.1.1.exe)
* TODO

### Known Issues
* Color matching rules might need a little bit of work
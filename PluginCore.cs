﻿#define VTC_PLUGIN
using System;
using System.Linq;
using Decal.Adapter;
using MyClasses.MetaViewWrappers;
using BotShopper.Lib;
using BotShopper.Views;
using System.IO;

namespace BotShopper {

    [WireUpBaseEvents]
    [MVView("BotShopper.mainView.xml")]
    [MVWireUpControlEvents]
    [FriendlyName("BotShopper")]

    public class PluginCore : PluginBase {
        internal TradeManager tradeManager;
        internal RuleManager ruleManager;
        internal MainView mainView;

        protected override void Startup() {
            try {
                Globals.Init(this, "BotShopper", Host, Core);
                Util.CreatePluginDirectories();

                mainView = new MainView();

                CoreManager.Current.CharacterFilter.LoginComplete += CharacterFilter_LoginComplete;
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void Current_RenderFrame(object sender, EventArgs e) {
            try {
                ChatBox.Think();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void CharacterFilter_LoginComplete(object sender, EventArgs e) {
            try {
                Directory.CreateDirectory(Util.GetCacheStoragePath());

                tradeManager = new TradeManager();
                ruleManager = RuleManager.LoadRules();

                ruleManager.Init();
                tradeManager.Init();
                mainView.Init();

                ChatBox.Init();

                CoreManager.Current.RenderFrame += Current_RenderFrame;
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        protected override void Shutdown() {
            try {
                CoreManager.Current.CharacterFilter.LoginComplete -= CharacterFilter_LoginComplete;
                CoreManager.Current.RenderFrame -= Current_RenderFrame;

                if (mainView != null) mainView.Dispose();
                if (tradeManager != null) tradeManager.Dispose();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}
 
 